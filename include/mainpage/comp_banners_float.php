<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?> <?global $isShowFloatBanner;?> <?if($isShowFloatBanner):?>
<div class="wrapper_inner1 wides float_banners">
	 <?$APPLICATION->IncludeComponent(
	"aspro:com.banners.next",
	"next",
	Array(
		"BANNER_TYPE_THEME" => "FLOAT",
		"BANNER_TYPE_THEME_CHILD" => "20",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CATALOG" => "/catalog/",
		"CHECK_DATES" => "Y",
		"FILTER_NAME" => "arRegionLink",
		"IBLOCK_ID" => "27",
		"IBLOCK_TYPE" => "aspro_next_adv",
		"NEWS_COUNT" => "9",
		"NEWS_COUNT2" => "20",
		"PROPERTY_CODE" => array("TEXT_POSITION","TARGETS","TEXTCOLOR","URL_STRING","BUTTON1TEXT","BUTTON1LINK","BUTTON2TEXT","BUTTON2LINK",""),
		"SET_BANNER_TYPE_FROM_THEME" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "ID",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "DESC",
		"TYPE_BANNERS_IBLOCK_ID" => "25"
	)
);?>
</div>
<div class="clearfix">
</div>
<?endif;?>