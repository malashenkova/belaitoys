<?require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");?>

<?
class GReCaptcha {
    public static function getKey() {
        return "6LdG4WgUAAAAAPtm8kXsNZp3ql8Ogx2-LSJkZBhH";
    }

    public static function getSecretKey() {
        return "6LdG4WgUAAAAAESihl5jY6_-SStQ2cBe8GSKUdXy";
    }
    
    function checkReCaptchaResponse() {
        $context = \Bitrix\Main\Application::getInstance()->getContext();
        $req = $context->getRequest();
        $g_recaptcha_response = $req->getPost("g-recaptcha-response"); 
        
        if($g_recaptcha_response) {
            $responseUrl = "https://www.google.com/recaptcha/api/siteverify";
            $param = array(
                'secret'=>GReCaptcha::getSecretKey(),
                'response'=>$g_recaptcha_response;
            );
            $client = new HttpClient();
            $request = $client->post($responseUrl, $param);
            if($request) {
                $request = \Bitrix\Main\Web\Json::decode($request);
                if(!$request['success']) {
                    return $request['error-codes'];
                }
                return array();
            }
            return array('no captcha');
        }
    }
}

?>