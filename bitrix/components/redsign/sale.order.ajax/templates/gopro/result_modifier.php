<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!CModule::IncludeModule('redsign.devfunc'))
	return;

// get no photo
$arResult['NO_PHOTO'] = RSDevFunc::GetNoPhoto(array('MAX_WIDTH'=>95,'MAX_HEIGHT'=>55));
// /get no photo

if(!CModule::IncludeModule('redsign.location'))
	return;
if(!CModule::IncludeModule('sale'))
	return;

$COM_SESS_PREFIX = "RSLOCATION";
$detectedLocID = 0;
$detectedLocID = IntVal($_SESSION[$COM_SESS_PREFIX]['LOCATION']['ID']);
$arResult['RSDETECTED_LOCATION_VALUE'] = '-';
if( $detectedLocID>0 )
{
	$arResult['RSDETECTED_LOCATION_VALUE'] = $detectedLocID;
} else {
	$detected = array();
	$detected = CRS_Location::GetCityName();
	
	if( isset($detected['CITY_NAME']) )
	{
		$dbRes = CSaleLocation::GetList(
			array('SORT'=>'ASC','CITY_NAME_LANG'=>'ASC'),
			array('LID'=>LANGUAGE_ID,'CITY_NAME'=>$detected['CITY_NAME'])
		);
		if($arFields = $dbRes->Fetch())
		{
			$arResult['RSDETECTED_LOCATION_VALUE'] = $arFields['ID'];
		}
	}
}
?>
<?if ($_SESSION['CURRENCY'] != ''){
	$discountPrice = 0;
	foreach ($arResult['GRID']['ROWS'] as $key => $arItem) {
		$arResult['GRID']['ROWS'][$key]['data']['PRICE'] = CCurrencyRates::ConvertCurrency($arResult['GRID']['ROWS'][$key]['data']['PRICE'], "USD", $_SESSION['CURRENCY']);
		$arResult['GRID']['ROWS'][$key]['data']['DISCOUNT_PRICE'] = CCurrencyRates::ConvertCurrency($arResult['GRID']['ROWS'][$key]['data']['DISCOUNT_PRICE'], "USD", $_SESSION['CURRENCY']);
		$discountPrice = $discountPrice + $arResult['GRID']['ROWS'][$key]['data']['DISCOUNT_PRICE']*$arResult['GRID']['ROWS'][$key]['data']['QUANTITY'];
		$arResult['GRID']['ROWS'][$key]['data']['CURRENCY'] = $_SESSION['CURRENCY'];
		$arResult['GRID']['ROWS'][$key]['data']['PRICE_FORMATED'] = CurrencyFormat($arResult['GRID']['ROWS'][$key]['data']['PRICE'], $_SESSION['CURRENCY']);
		$arResult['GRID']['ROWS'][$key]['data']['SUM'] = CurrencyFormat($arResult['GRID']['ROWS'][$key]['data']['PRICE']*$arResult['GRID']['ROWS'][$key]['data']['QUANTITY'], $_SESSION['CURRENCY']);
	}
	$arResult['ORDER_PRICE'] = CCurrencyRates::ConvertCurrency($arResult['ORDER_PRICE'], 'USD', $_SESSION['CURRENCY']);
	$arResult['ORDER_PRICE_FORMATED'] = CurrencyFormat($arResult['ORDER_PRICE'], $_SESSION['CURRENCY']);
	$arResult['ORDER_TOTAL_PRICE_FORMATED'] = CurrencyFormat($arResult['ORDER_PRICE'], $_SESSION['CURRENCY']);
	$arResult['PRICE_WITHOUT_DISCOUNT'] = CurrencyFormat($arResult['ORDER_PRICE'] + $discountPrice, $_SESSION['CURRENCY']);
}else{
	$discountPrice = 0;
	foreach ($arResult['GRID']['ROWS'] as $key => $arItem) {
		$arResult['GRID']['ROWS'][$key]['data']['PRICE'] = CCurrencyRates::ConvertCurrency($arResult['GRID']['ROWS'][$key]['data']['PRICE'], "USD", 'BYR');
		$arResult['GRID']['ROWS'][$key]['data']['DISCOUNT_PRICE'] = CCurrencyRates::ConvertCurrency($arResult['GRID']['ROWS'][$key]['data']['DISCOUNT_PRICE'], "USD", 'BYR');
		$discountPrice = $discountPrice + $arResult['GRID']['ROWS'][$key]['data']['DISCOUNT_PRICE']*$arResult['GRID']['ROWS'][$key]['data']['QUANTITY'];
		$arResult['GRID']['ROWS'][$key]['data']['CURRENCY'] = $_SESSION['CURRENCY'];
		$arResult['GRID']['ROWS'][$key]['data']['PRICE_FORMATED'] = CurrencyFormat($arResult['GRID']['ROWS'][$key]['data']['PRICE'], 'BYR');
		$arResult['GRID']['ROWS'][$key]['data']['SUM'] = CurrencyFormat($arResult['GRID']['ROWS'][$key]['data']['PRICE']*$arResult['GRID']['ROWS'][$key]['data']['QUANTITY'], 'BYR');
	}
	$arResult['ORDER_PRICE'] = CCurrencyRates::ConvertCurrency($arResult['ORDER_PRICE'], 'USD', 'BYR');
	$arResult['ORDER_PRICE_FORMATED'] = CurrencyFormat($arResult['ORDER_PRICE'], 'BYR');
	$arResult['ORDER_TOTAL_PRICE_FORMATED'] = CurrencyFormat($arResult['ORDER_PRICE'], 'BYR');
	$arResult['PRICE_WITHOUT_DISCOUNT'] = CurrencyFormat($arResult['ORDER_PRICE'] + $discountPrice, 'BYR');
}?>
