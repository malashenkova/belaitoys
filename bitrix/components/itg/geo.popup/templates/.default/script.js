var marker = true;
if(marker) {
	$(document).ready(function () {
		var startRegion = $(".dotted").text();
		$(".contacts-stores h4").text(startRegion);
		marker = false;
	});
}

function hide() {
	$(".select-region").addClass("hidden");
	$(".list-region").addClass("hidden");
	$.ajax({
		url:"/bitrix/components/itg/geo.popup/ajax/close.php",
		type:'POST',
		data:{select_region_close: 'yes'},
	});
}
function display() {
	$(".select-region").removeClass("hidden");
}
function displayList() {
	$(".list-region").removeClass("hidden");
}
function changeRegion(regionName) {
	console.log(regionName);
	$.ajax({
		url:"/bitrix/components/itg/geo.popup/ajax/change_region.php",
		type:'POST',
		data:{changeRegion: regionName},
		success: function(data){
      		$(".dotted").empty();
      		$(".dotted").append(data);
      		$(".your-region").empty();
      		$(".your-region").append(data);
      		$(".all-region li").removeClass("selected");
      		$(".region-item").addClass("selected");
      		var change = $(".region-item .selected").text();
      		console.log($(".contacts-stores h4").text(change));
      		$(".contacts-stores h4").text(change);
        }        
	});
            
	$(".list-region").addClass("hidden");
}