<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
$this->setFrameMode(true);?>
<?session_start();?>
<?if(!empty($arResult["REGION_GEOLOCATION"]) && !empty($arResult["REGION_LIST"])):?>
	<div class="geo-region">
		<p class="region-name"><img class="icon-geo" src="<?=$templateFolder?>/images/GEO_target.png"><a class="dotted" href="javascript:void(0)" onclick="display()"><?=$_SESSION["regionName"]?></a></p>
	</div>
	<div class="select-region <?//if($_SESSION['select_region_close'] == 'yes') echo "hidden";?>">
		<div class="content-region">
			<div class="geo-comtent">
				<div><?=Loc::getMessage("REGION")?> <p class="region-name your-region"><?=$_SESSION["regionName"]?>?</p></div>
			</div>
			<div class="button_geo">
				<a href="javascript:void(0)" class="confirm-region" onclick="hide()"><?=Loc::getMessage("CONFIRM_BUTTON")?></a>
				<a href="javascript:void(0)" class="open-list-region" onclick="displayList()"><?=Loc::getMessage("CHOICE_REGION")?></a>
			</div> 
		</div>
	</div>
	<div class="list-region hidden">
		<h4><?=Loc::getMessage("LIST_REGION_TITLE")?></h4>
		<ul class="all-region">
			<?foreach($arResult["REGION_LIST"] as $key => $value):?>
				<?if(strlen($value["NAME"]) > 0):?>
					<li data-value="<?=$value["ID"]?>">
						<a href="javascript:void(0)" class="region-item" onclick="changeRegion('<?=$value["NAME"]?>')"><?=$value["NAME"]?></a>
					</li>
				<?endif;?>
			<?endforeach;?>
		</ul>
	</div>
<?endif;?>