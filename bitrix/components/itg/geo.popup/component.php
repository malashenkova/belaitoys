<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
use \Bitrix\Main\Service\GeoIp;
global $APPLICATION;
session_start();

$ipAddress = GeoIp\Manager::getRealIp();
$lang = "ru";
$result = GeoIp\Manager::getDataResult($ipAddress, $lang);
$userGeo = $result->getGeoData();
$arResult["REGION_GEOLOCATION"] = $userGeo->regionName;
if(!$_SESSION["regionName"]) {
	$_SESSION["regionName"] = $arResult["REGION_GEOLOCATION"];
}

$cacheTime = 86400;
if($this->StartResultCache($cacheTime)) {
	if(Bitrix\Main\Loader::includeModule('sale')) {
		$arOrder = array("SORT" => "ASC");
		$arFilter = array("IBLOCK_ID" => 34);
		/*array("COUNTRY_ID" => "1")*/
		$arSelectFields = array("ID", "NAME");
		$regionList = CIBlockSection::GetList($arOrder, $arFilter, false, $arSelectFields);
		/*CSaleLocation::GetList($sort, $arFilter, false, false, $arSelectFields);*/
		while ($result = $regionList->fetch()) {
			$arResult["REGION_LIST"][] = $result;
		}
	}
}
$this->IncludeComponentTemplate();
?>