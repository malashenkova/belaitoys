<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php"); 
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/iblock/iblock.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/iblock/prolog.php"); 
CJSCore::Init(array("jquery"));

IncludeModuleLangFile(__FILE__);  
IncludeTemplateLangFile(__FILE__); 

if(!$USER->IsAdmin()) $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED")); 
CModule::IncludeModule('iblock');

$arErrors = array(); 
$arMessages = array();  
$arTabs = array();

$APPLICATION->SetTitle(GetMessage("PAGE_TITLE")); 
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
$arTabs = Array(  
	Array( 
		"DIV" => "edit_elements",  
		"TAB" => "Экспорт прайс листа каталога (csv)", 
		"ICON" => "main_user_edit", 
		"TITLE" => "Экспорт прайс листа каталога (csv)",  
	) 
);  
$tabControl = new CAdminTabControl("tabControl", $arTabs, true, true);
$tabControl->Begin(); 
$tabControl->BeginNextTab();  
?>
<!-- todo :: импорт каталога --> 
<?$APPLICATION->IncludeComponent("itg:export_catalog_price","",Array())?>
<?  
$tabControl->End(); 
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>
