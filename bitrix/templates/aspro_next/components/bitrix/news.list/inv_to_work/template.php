<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
session_start();
?>
<div class="overlay <?if($_SESSION['close'] == 'yes') echo "closed";?>">
	<div class="inv-to-work <?if($_SESSION['close'] == 'yes') echo "closed";?>">
		<?foreach($arResult["ITEMS"] as $arItem):?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>	
				<?if($arItem["PROPERTIES"]["CLOSE_ICON"]["VALUE"]):?>
					<div class="close-button"><a href="javascript:void(0)" onclick="closeBanner();"><img src="<?=$arItem["PROPERTIES"]["CLOSE_ICON"]["VALUE"]?>"></a></div> 
				<?endif;?>	
				<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
					<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
						<p class="name-inv"><b><?=$arItem["NAME"]?></b></p>
					<?else:?>
						<p class="name-inv"><b><?=$arItem["NAME"]?></b><p>
					<?endif;?>
				<?endif;?>
				<br>
				<?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
					<p class="mess-inv">
					<?if(is_array($arProperty["DISPLAY_VALUE"])):?>
						<?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
					<?else:?>
						<?=$arProperty["DISPLAY_VALUE"];?>
					<?endif;?>
					</p>
				<?endforeach;?>
		<?endforeach;?>
	</div>
</div>

