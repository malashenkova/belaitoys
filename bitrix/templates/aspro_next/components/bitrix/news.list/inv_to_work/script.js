function closeBanner() {
	$.ajax({
		url:"/ajax/close_banner.php",
		dataType:"json",
		type:'POST',
		data:{close: 'yes'},
	}).done(function(data) {
		var result = JSON.parse(data);
		if(result == true) {
			$('.overlay').css({'display': 'none'});
			$('.inv-to-work').css({'bottom':'-20%', 'height':'10%'});
		} 
	});
}

$(document).ready(function(){
	function showModal() {
	    $('.inv-to-work').fadeIn('fast', function() {
	        setTimeout(showModal, 1000);
	    });
	}
});
