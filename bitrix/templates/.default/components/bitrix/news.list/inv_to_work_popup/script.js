function closeBanner() {
	$.ajax({
		url:"/ajax/close_banner.php",
		dataType:"json",
		type:'POST',
		data:{close: 'yes'},
	}).done(function(data) {
		var result = JSON.parse(data);
		if(result == true) {
			$('.inv-to-work').css({'bottom':'-20%', 'height':'10%'});
		} 
	});
}
