<?header('Content-type: application/json');
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

	CModule::IncludeModule('sale');
	CSaleBasket::Delete($_POST['id']);

	$est = CSaleBasket::GetList(false, array("FUSER_ID" => CSaleBasket::GetBasketUserID(),"LID" => SITE_ID,"ORDER_ID" => "NULL"),false,false,array("ID" ))->SelectedRowsCount();
	$est = $est - 1;
	
	$arResult['left'] = $est;
  	echo json_encode($arResult);
?>