<?
define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/log.txt");
function dump($var, $die = false, $all = false)
{
    global $USER;
    if( ($USER->GetID() == 1) || ($all == true))
    {
        ?>
        <font style="text-align: left; font-size: 10px"><pre><?var_dump($var)?></pre></font><br>
        <?
    }
    if($die)
    {
        die;
    }
}
AddEventHandler('main', 'OnBuildGlobalMenu', Array('BXMenu', 'OnBuild'));
class BXMenu{
    function OnBuild(&$aGlobalMenu, &$aModuleMenu){        
        global $USER;
        
        if($USER->IsAdmin()){
            
            foreach($aModuleMenu as $key=>$items){            
                if($items["text"] == '1С Каталог'){
                    $aModuleMenu[$key]["items"][] = array(
                        'text' => 'Экспорт прайс листа каталога (csv)',
                        'title' => 'Экспорт прайс листа каталога (csv)',
                        'url' => 'export_catalog_price.php',
                        'sort' => 1000
                    );
                }
            }
        }
    }
}



AddEventHandler("sale", "OnOrderNewSendEmail", Array("MyClass", "OnOrderNewSendEmail"));
AddEventHandler("iblock", "OnBeforeIBlockSectionUpdate", Array("MyClass", "OnBeforeIBlockSectionUpdateHandler"));
class MyClass extends CModule{
    
    function OnBeforeIBlockSectionUpdateHandler(&$arFields)
    {
        
        // global $APPLICATION;
        
        // $trans = CUtil::translit($arFields["NAME"],"ru",array("replace_space"=>"_","replace_other"=>"-"));
        // $arFields["CODE"] = $trans;
        // dump($arFields);
        // $APPLICATION->throwException("Введите символьный код. (ID:".$arFields["ID"].")");
        // return false;
        
    }

    public function OnOrderNewSendEmail($ID, $eventName, $arFields){

        if(SITE_ID == "s1") {

        CModule::IncludeModule('sale');
        CModule::IncludeModule('iblock');
        CModule::IncludeModule('main');

        $arFields["ORDER_LIST"] = false;

        $dbBasket = CSaleBasket::GetList(Array("ID"=>"ASC"), Array("ORDER_ID"=>$ID));

        $total_sum = '';
        $arOrder = CSaleOrder::GetByID($ID);

        while($basketElements = $dbBasket->GetNext()){

            $elements[$basketElements["PRODUCT_ID"]] = $basketElements;
            $itemsID[] = $basketElements["PRODUCT_ID"];
            
            $elements[$basketElements["PRODUCT_ID"]]["TOTAL_SUM"] = $basketElements["PRICE"] * $basketElements["QUANTITY"];
            #$elements[$basketElements["PRODUCT_ID"]]["DISCOUNT_PERCENT"] = round(( intval($basketElements["DISCOUNT_PRICE"]) * 100 ) / intval($basketElements["BASE_PRICE"]));
            $elements[$basketElements["PRODUCT_ID"]]["DISCOUNT_PERCENT"] = $basketElements["DISCOUNT_VALUE"];
            $total_sum = $total_sum + $basketElements["PRICE"] * $basketElements["QUANTITY"];
        }
        
        $arImages = CIBlockElement::GetList(array("ID" => "ASC"), array("ID" => $itemsID));
        
        while($image = $arImages->GetNext()){
            $img = $file = CFile::ResizeImageGet($image['DETAIL_PICTURE'], array('width'=>100, 'height'=>100), BX_RESIZE_IMAGE_PROPORTIONAL, true);
            $images[$image["ID"]] = $img['src'];
        }     

       
        $total_string = '
            <table cellpadding="0" cellspacing="0" class="header">
                <thead style="color:#000; font-size:8px;">
                    <tr>
                        <td style="width: 20%; text-align:right; padding-bottom:10px; ">
                             Товары
                        </td>
                        <td style="width: 30%; padding-bottom:10px; ">
                        </td>                       
                        <td style="width: 15%; padding-bottom:10px; ">
                             Цена
                        </td>
                        <td style="width: 10%; padding-bottom:10px; ">
                             Количество
                        </td>
                        <td style="width: 15%; padding-bottom:10px; ">
                             Сумма
                        </td>
                    </tr>
                </thead>
            <tbody style="text-align:left;font-size:11px">';

        
        foreach ($elements as $key => $item) {
            
            $total_string .= '
            <tr>
                <td style="width: 20%; padding-bottom: 10px;text-align:center;">
                    <img width="70" src="http://'.$_SERVER['SERVER_NAME'].$images[$key].'" alt="">
                </td>
                <td style="width: 30%; padding-bottom: 10px;">
                     <a href="http://'.$_SERVER['SERVER_NAME'].$item["DETAIL_PAGE_URL"].'">'.$item["NAME"].'</a>
                </td>
                <td style="width:15%; padding-bottom: 10px;text-align:center;">
                     '.round($item["PRICE"], 2).' руб.
                </td>
                <td style="width: 10%; padding-bottom: 10px;text-align:center;">
                     '.$item["QUANTITY"].'
                </td>
                <td style="width: 15%; padding-bottom: 10px;text-align:center;">
                     '.round($item["TOTAL_SUM"], 2).' руб.
                </td>
            </tr>';
        }

        $total_string .= '</tbody>
            </table>
            <table cellpadding="0" cellspacing="0" class="header" style="width: 100; margin: 10px 0;">
            <tbody>
            <tr>
                <td style="font-size:9px; color:#000; padding-left: 10px;">
                     Сумма заказа: <b>'.round($total_sum, 2).' руб.</b>
                </td>
            </tr>
            </tbody>
            </table>';
            
        if( strlen($arOrder["USER_DESCRIPTION"]) > 0 ){
            $total_string .= '
                <p style="text-align:left">Коментарий к заказу<p>
                <p style="text-align:justify">'.$arOrder["USER_DESCRIPTION"].'</p>
            ';
        }
        
        if(!$arFields["SITE_NAME"]) $arFields["SITE_NAME"] = 'ООО "ТАВ-Инвест"';
        $total_sum_formated = round($total_sum, 2).' руб.';
        $arEventFields = array(
            "ORDER_ID"                  => $ID,
            "ORDER_DATE"             => $arFields["ORDER_DATE"],
            "ORDER_USER"             => $arFields["ORDER_USER"],
            "ORDER_LIST_STRING"            => $total_string,
            "DUPLICATE"     => "Y",
            "SITE_NAME" => $arFields["SITE_NAME"],
            "EMAIL"         => $arFields["EMAIL"],    
            "PRICE" =>       $total_sum_formated,
        );
        
        CEvent::Send($eventName, s1, $arEventFields);
        
        return false;
        }
        else {
            return true;
        }
    }
}

function fieldsToLog($arFields, $module){
   
    foreach($arFields as $k=>$item){
        $mess .= "[".$k."] - ".$item.";"."\n";
        if(is_array($item)){
            $mess .= "{"."\n";
            foreach($item as $key=>$i){
                $mess .= "\t"."[".$key."] - ".$i.";"."\n";
                if(is_array($i)){
                    $mess .= "\t"."{"."\n";
                    foreach($i as $key_it=>$it){
                        $mess .= "\t\t"."[".$key_it."] - ".$it.";"."\n";
                        if(is_array($it)){
                            $mess .= "\t\t"."{"."\n";
                            foreach($it as $key_ite=>$ite){
                                $mess .= "\t\t\t"."[".$key_ite."] - ".$ite.";"."\n";
                                if(is_array($ite)){
                                    $mess .= "\t\t\t"."{"."\n";
                                    foreach($ite as $kp=>$prop){
                                        $mess .= "\t\t\t\t"."[".$kp."] - ".$prop.";"."\n";
                                    }
                                    $mess .= "\t\t\t"."}"."\n";
                                }
                            }
                            $mess .= "\t\t"."}"."\n";
                        }
                    }
                    $mess .= "\t"."}"."\n";
                }
            }
            $mess .= "}"."\n";
        }
    }
    AddMessage2Log($mess, $module); 
}

function updateCatalogNewTags() {
    CModule::IncludeModule('iblock');
    define("PRIMATOYS_IBLOCK_ID", 41);

    $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*");
    $arFilter = Array("IBLOCK_ID"=>PRIMATOYS_IBLOCK_ID, "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    while($ob = $res->GetNextElement()){ 
        $arFields = $ob->GetFields();  
        
        $arProps = $ob->GetProperties();
        $NEWTag[$arFields["ID"]] = array();

        if($arProps["NOVINKA"]["VALUE"] == "Да") {
            $NEWTag[$arFields["ID"]][] = "6370";
        }
        if($arProps["KHIT_PRODAZH"]["VALUE"] == "Да") {
            $NEWTag[$arFields["ID"]][] = "6368";
        }
    }
    foreach($NEWTag as $CODE => $VALUE) {

        CIBlockElement::SetPropertyValueCode($CODE, "HIT", $VALUE);
        /*
            [0] => HIT
            [1] => RECOMMEND
            [2] => NEW
            [3] => STOCK

            [0] => 6368
            [1] => 6369
            [2] => 6370
            [3] => 6371
        */
    };
    return "updateCatalogNewTags();";
}

// 

// class MyClass
// {
//     // создаем обработчик события "OnBeforeIBlockElementUpdate"
//     
// }