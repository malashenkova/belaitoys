<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="REFRESH" content="10; url=https://belaitoys.ru">
		<title>Мы переехали!</title>
		<style type="text/css">
			html, body {
				margin: 0;
				padding: 0;
			}
			body {
				display: flex;
				min-height: 100vh;
				align-items: center;
				justify-content: center;
			}
			img {
				max-width: 100%;
			}
		</style>
	</head>
	<body>
		<a href="https://belaitoys.ru">
			<img src="redirect.jpg" alt="redirect_logo"/>
		</a>
	</body>
</html>