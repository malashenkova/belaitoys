<?
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
	$APPLICATION->SetTitle("Регистрация");
	// if(!$USER->IsAuthorized()){?>	
	<?		
		$APPLICATION->IncludeComponent(
			"itg:main.register", 
			"main", 
			array(
				"USER_PROPERTY_NAME" => "",
				"SHOW_FIELDS" => array(
					0 => "EMAIL",
					1 => "NAME",
					2 => "PERSONAL_PHONE",
					3 => "PERSONAL_CITY",
				),
				"REQUIRED_FIELDS" => array(
					0 => "EMAIL",
					1 => "NAME",
					2 => "PERSONAL_PHONE",
					3 => "PERSONAL_CITY",
				),
				"AUTH" => "Y",
				"USE_BACKURL" => "Y",
				"SUCCESS_PAGE" => "",
				"SET_TITLE" => "N",
				"USER_PROPERTY" => array(
				),
				"COMPONENT_TEMPLATE" => "main"
			),
			false
		);
	// 	$_REQUEST["REGISTER[LOGIN]"] = $_REQUEST["REGISTER[EMAIL]"];
	// } elseif(!empty( $_REQUEST["backurl"] )) {
	// 	LocalRedirect( $_REQUEST["backurl"] );
	// } else { 
	// 	LocalRedirect(SITE_DIR.'personal/');
	// }

	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>