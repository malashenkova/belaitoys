<?
	header('Content-type: application/json');
    require_once($_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/main/include/prolog_before.php');
    
    global $startExecTime, $DB, $USER;
    CModule::IncludeModule('iblock');

    define("PRIMATOYS_IBLOCK_ID", 41);
    define("PRIMATOYS_PRICE_ID", 30);
    function checkExecutionTime(){
        global $startExecTime;
        $result = true;
        $estTime = time() - $startExecTime;
        if($estTime > 45) $result = false;
        return $result;
    }
    
    $startExecTime = time();
    
    $DB->Query('
        CREATE TABLE IF NOT EXISTS `itg_primatoys_catalog_export_sections` (
            ID INT NOT NULL PRIMARY KEY,
            IBLOCK_ID INT NOT NULL,
            IBLOCK_SECTION_ID INT NOT NULL,
            DEPTH_LEVEL INT NOT NULL,
            LAST_SECTION INT NOT NULL,
            SECTION_PAGE_URL VARCHAR(200) NOT NULL,
            SECTION_NAME VARCHAR(200) NOT NULL,
            STATUS INT NOT NULL
        )
    ');
    /*$DB->Query('DROP TABLE `itg_primatoys_catalog_export_elements`');
    exit;*/
    $DB->Query('
        CREATE TABLE IF NOT EXISTS `itg_primatoys_catalog_export_elements` (
            ID INT NOT NULL PRIMARY KEY,
            IBLOCK_ID INT NOT NULL,
            IBLOCK_SECTION_ID INT NOT NULL,
            ACTIVE VARCHAR(10) NOT NULL,
            NAME VARCHAR(200) NOT NULL,
            PICTURE VARCHAR(200) NOT NULL,
            DETAIL_PAGE_URL VARCHAR(200) NOT NULL,
            ARTNUMBER VARCHAR(200) NOT NULL,
            PRICE VARCHAR(200) NOT NULL,
            STATUS INT NOT NULL
        )
    ');
    // $DB->Query("ALTER TABLE itg_catalog_export_elements ADD PRICE VARCHAR(200) NOT NULL");
    $DB->Query("TRUNCATE TABLE itg_primatoys_catalog_export_elements");
    $DB->Query("TRUNCATE TABLE itg_primatoys_catalog_export_sections");
    
    // проверим на наличие записей в таблице, если нет то зальем данные
    $rowsCount = $DB->Query("SELECT STATUS FROM itg_primatoys_catalog_export_sections WHERE STATUS = 0");

    if(checkExecutionTime() && $rowsCount->SelectedRowsCount() == 0){        
        // запишем во временную таблицу данные по разделам        
        $db_catalog = CIBlockSection::GetList(
            array("left_margin"=>"asc"),
            array("IBLOCK_ID" => PRIMATOYS_IBLOCK_ID, "ACTIVE" => "Y"),
            false,
            array(
                "ID","IBLOCK_ID", "IBLOCK_SECTION_ID", "DEPTH_LEVEL", "SECTION_PAGE_URL", "LEFT_MARGIN", "RIGHT_MARGIN", "NAME"
            )
        );

        while($result = $db_catalog->GetNext()){
            
            if(intval($result["RIGHT_MARGIN"]) - intval($result["LEFT_MARGIN"]) == 1) $lastSection = 1;
            else $lastSection = 0;
            
            $DB->Query("INSERT INTO itg_primatoys_catalog_export_sections SET
                ID = '".$DB->ForSql($result['ID'])."',
                IBLOCK_ID = '".$DB->ForSql($result['IBLOCK_ID'])."',
                IBLOCK_SECTION_ID = '".$DB->ForSql($result['IBLOCK_SECTION_ID'])."',
                DEPTH_LEVEL = '".$DB->ForSql($result['DEPTH_LEVEL'])."',
                SECTION_PAGE_URL = '".$DB->ForSql($result['SECTION_PAGE_URL'])."',
                LAST_SECTION = '".$DB->ForSql($lastSection)."',
                SECTION_NAME = '".$DB->ForSql($result['NAME'])."'
            ");

        }           
    }
    // запишем во временную таблицу товары
    // проверим на наличие записей в таблице, если нет то зальем данные
    $rowsCount = $DB->Query("SELECT STATUS FROM itg_primatoys_catalog_export_elements WHERE STATUS = 0");
    if(checkExecutionTime() && $rowsCount->SelectedRowsCount() == 0){
        $db_elements = CIBlockElement::GetList(
            array(),
            array(
                "IBLOCK_ID" => PRIMATOYS_IBLOCK_ID, "ACTIVE" => "Y", "INLUDE_SUBSETIONS" => 'Y'
            ),
            false,
            false,
            array(
                "ID","IBLOCK_ID", "IBLOCK_SECTION_ID", "NAME",
                "ACTIVE", "PREVIEW_PICTURE", "DETAIL_PICTURE",
                "DETAIL_PAGE_URL", "PROPERTY_CML2_ARTICLE"
            )
        );
        
        while($result = $db_elements->GetNext()){
            
            //$productQuantity = CCatalogProduct::GetByID($result["ID"])["QUANTITY"];
            $image = "";
            if($result["DETAIL_PICTURE"]) $image = CFile::GetPath($result["DETAIL_PICTURE"]);
            elseif($result["PREVIEW_PICTURE"]) $image = CFile::GetPath($result["PREVIEW_PICTURE"]);
            
            $resPrice = CCatalogProduct::GetOptimalPrice($result["ID"],PRIMATOYS_PRICE_ID,$USER->GetUserGroupArray(),"N")["RESULT_PRICE"]["DISCOUNT_PRICE"]; 
            
            $DB->Query("INSERT INTO itg_primatoys_catalog_export_elements SET
                ID = '".$DB->ForSql($result['ID'])."',
                IBLOCK_ID = '".$DB->ForSql($result['IBLOCK_ID'])."',
                IBLOCK_SECTION_ID = '".$DB->ForSql($result['IBLOCK_SECTION_ID'])."',
                ACTIVE = '".$DB->ForSql($result['ACTIVE'])."',
                NAME = '".$DB->ForSql($result['NAME'])."',
                PICTURE = '".$DB->ForSql($image)."',
                DETAIL_PAGE_URL = '".$DB->ForSql($result['DETAIL_PAGE_URL'])./*"',
                QUANTITY = '".$DB->ForSql($productQuantity).*/"',
                ARTNUMBER = '".$DB->ForSql($result["PROPERTY_CML2_ARTICLE_VALUE"])."',
                PRICE = '".$DB->ForSql($resPrice)."'
            ");
            
        }
    }

    // приступаем к записи в файл

    // структура файла
    // Артикул - Название -  Цена
    $db_sect = $DB->Query("SELECT * FROM itg_primatoys_catalog_export_sections WHERE STATUS = 0 ORDER BY ID ASC");
    $finalArray = array();
    while($result = $db_sect->Fetch()){
        if($result["LAST_SECTION"] == '0'){
            $finalArray[] = array(NULL, $result["SECTION_NAME"], NULL, NULL);
        }else{
            
            $finalArray[] = array(NULL, $result["SECTION_NAME"], NULL, NULL);
            $finalArray[] = array("Артикул", "Наименование", "Цена"/*, "Остаток товара"*/);
            $sectionElements = $DB->Query("
                SELECT * FROM itg_primatoys_catalog_export_elements
                WHERE IBLOCK_SECTION_ID = '".$result["ID"]."' AND STATUS = 0 ORDER BY NAME
            ");
            
            while($element = $sectionElements->Fetch()){
                //$available = floatval($element["QUANTITY"]) > 0 ? floatval($element["QUANTITY"])/*"В наличии"*/ : "Нет в наличии";
                if (!empty($element["PICTURE"])) {
                    $picture_link = '=ГИПЕРССЫЛКА("http://'.$_SERVER["SERVER_NAME"].$element["PICTURE"].'"; "'.str_replace('&quot;', "'", $element["NAME"]).'")';
                } else {
                    $picture_link = str_replace('&quot;', "'", $element["NAME"]);
                }
                $price = str_replace(".", ",", $element["PRICE"]);
                $finalArray[] = array(                    
                    $element["ARTNUMBER"],                                        
                    /*'=ГИПЕРССЫЛКА("http://'.$_SERVER["SERVER_NAME"].$element["DETAIL_PAGE_URL"].'"; "'.str_replace('&quot;', "'", $element["NAME"]).'")',*/
                    $picture_link,
                    $price/*,
                    $available*/
                );
            }
        }
    }
    // перекодируем в 1251
    foreach($finalArray as $key => $items){
        foreach($items as $k => $item){
            $finalArray[$key][$k] = iconv("UTF-8", "WINDOWS-1251", $item);
        }
    }
    $filename = $USER->GetLogin().md5($USER->GetEmail()).".csv";
    $file = $_SERVER["DOCUMENT_ROOT"]."/upload/prices/".$filename;
    $fp = fopen($file, 'w');
    foreach ($finalArray as $fields) {
        fputcsv($fp, $fields, ";");
    }
    fclose($fp);
    
    $arResult["file"] = "/upload/prices/".$filename;

    echo json_encode($arResult);