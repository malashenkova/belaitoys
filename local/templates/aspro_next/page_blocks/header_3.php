<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?
global $arTheme, $arRegion;
$arRegions = CNextRegionality::getRegions();
if($arRegion)
	$bPhone = ($arRegion['PHONES'] ? true : false);
else
	$bPhone = ((int)$arTheme['HEADER_PHONES'] ? true : false);
$logoClass = ($arTheme['COLORED_LOGO']['VALUE'] !== 'Y' ? '' : ' colored');
?>
<div class="header-v4 header-wrapper">
	<div class="logo_and_menu-row">
		<div class="logo-row">
			<div class="maxwidth-theme">
				<div class="row">
					<div class="logo-block col-md-2 col-sm-3">
						<div class="logo<?=$logoClass?>">
							<?=CNext::ShowLogo();?>
						</div>
					</div>
					<?if($arRegions):?>
						<div class="inline-block pull-left">
							<div class="top-description">
								<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
									array(
										"COMPONENT_TEMPLATE" => ".default",
										"PATH" => SITE_DIR."include/top_page/regionality.list.php",
										"AREA_FILE_SHOW" => "file",
										"AREA_FILE_SUFFIX" => "",
										"AREA_FILE_RECURSIVE" => "Y",
										"EDIT_TEMPLATE" => "include_area.php"
									),
									false
								);?>
							</div>
						</div>
					<?endif;?>

					<div class="col-md-<?=($arRegions ? 2 : 3);?> col-lg-<?=($arRegions ? 2 : 3);?> search_wrap">
						<div class="search-block inner-table-block">
							<?$APPLICATION->IncludeComponent(
								"bitrix:main.include",
								"",
								Array(
									"AREA_FILE_SHOW" => "file",
									"PATH" => SITE_DIR."include/top_page/search.title.catalog.php",
									"EDIT_TEMPLATE" => "include_area.php"
								)
							);?>
						</div>
					</div>
					<div class="right-icons pull-right">
						<div class="pull-right block-link">
							<?=CNext::ShowBasketWithCompareLink('with_price', 'big', true, 'wrap_icon inner-table-block baskets');?>
						</div>
						<div class="pull-right">
							<div class="wrap_icon inner-table-block">
								<?=CNext::showCabinetLink(true, true, 'big', true);?>
							</div>
							<?/*
							<div class="msng_block">
								<a href="skype:+79206660502?call"><img src="<?=$APPLICATION->GetTemplatePath("images/msg_icon/skype.png");?>" border="0"></a>
								<a title="Viber" href="viber://chat?number=+79206660502"><img src="<?=$APPLICATION->GetTemplatePath("images/msg_icon/viber.png");?>" border="0"></a>
								<a title="WhatsApp" href="whatsapp://send?phone=+79206660502"><img src="<?=$APPLICATION->GetTemplatePath("images/msg_icon/whatsapp.png");?>" border="0"></a>
							</div>
							*/?>
						</div>
						<?if($bPhone):?>
							<div class="pull-right">
								<div class="wrap_icon inner-table-block">
									<div class="phone-block">
										<?CNext::ShowHeaderPhones();?>
										<?if($arTheme['SHOW_CALLBACK']['VALUE'] == 'Y'):?>
											<div class="callback-block">
												<span class="animate-load twosmallfont colored" data-event="jqm" data-param-form_id="CALLBACK" data-name="callback"><?=GetMessage("CALLBACK")?></span>
											</div>
										<?endif;?>
									</div>
								</div>
							</div>
						<?endif?>
					</div>
				</div>
			</div>
		</div><?// class=logo-row?>
	</div>
	<div class="menu-row middle-block bg<?=strtolower($arTheme["MENU_COLOR"]["VALUE"]);?>">
		<div class="maxwidth-theme">
			<div class="row">
				<div class="col-md-12">
					<div class="menu-only">
						<nav class="mega-menu sliced">
							<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
								array(
									"COMPONENT_TEMPLATE" => ".default",
									"PATH" => SITE_DIR."include/menu/menu.top.php",
									"AREA_FILE_SHOW" => "file",
									"AREA_FILE_SUFFIX" => "",
									"AREA_FILE_RECURSIVE" => "Y",
									"EDIT_TEMPLATE" => "include_area.php"
								),
								false, array("HIDE_ICONS" => "Y")
							);?>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?/*global $USER;
								if($USER->IsAdmin()) {
									?><div style="margin-left: 78%; margin-top: 2%;"><?
									$APPLICATION->IncludeComponent(
									"itg:geo.popup",
									".default",
									Array(
									),
									false
									);
									?></div><?
								}*/?>
	<div class="line-row visible-xs"></div>
</div>