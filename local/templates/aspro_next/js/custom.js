/*
You can use this file with your scripts.
It will not be overwritten when you upgrade solution.
*/
$(document).ready(function(){
    $("li[data-value='825']").click();
    var curRegion = "";
    $(function(){
        ymaps.ready(init);
        function init() {
          var geolocation = ymaps.geolocation;
          
          if (geolocation) {
            console.log(geolocation.region);
            curRegion = geolocation.region;
          } else {
            console.log('Не удалось установить местоположение');
          }
        }
        curRegion = "Смоленская обл.";
    });
    function setRegion() {
        if(curRegion != "") {
            setTimeout(function(){

                
                /*$(".ik_select_link .trigger").click();
                console.log("1");
                setTimeout(function(){
                    $(".ik_select_option").removeClass("ik_select_active");
                    $(".ik_select_option").removeClass("ik_select_hover");
                    $("span:contains('Смоленская обл.')").parent().addClass("ik_select_hover");
                    console.log("2");
                    setTimeout(function(){
                        $("span:contains('Смоленская обл.')").parent().addClass("ik_select_active");
                        console.log("3");
                        setTimeout(function(){
                            //$("span:contains('Смоленская обл.')").parent().click(); 
                            $("li.ik_select_option.ik_select_active.ik_select_hover").click();
                            console.log("4");
                            console.log("Clicked: "+curRegion);
                        }, 1000);
                    }, 1000);
                }, 1000);*/
                



               $.ajax({
                method: "POST",
                url: window.location,
                data: { ID: 825 }
              })
                .done(function( msg ) {
                    $(".item-views-wrapper.contacts").html("");
                  $(".contacts-page-map").html(msg);
                  $(".ik_select_link_text").html($(".item-views-wrapper.contacts h4").html());
                });

            }, 50);
            
            
            
               
            
        } else {
            console.log("oh no!");
        };
      }
      
      setTimeout(setRegion, 10);
    
    // Ajax item change on section
    $(".same_elements.same_elements_small a").click(function(e){
        e.preventDefault();
        var parentItem = $(this).closest(".catalog_item");
        $.ajax({
            url: '/ajax/same_items.php',
            dataType : "json",
            data: {ID: $(this).attr("data-id"), action: 'sameChange', place: 'section'},
            success: function (data) {
                console.log(data);
                
                $.each(data, function(i, val) {
                    
                    if(i == "NAME") parentItem.find(".item-title a span").html(val);
                    if(i == "DETAIL_PAGE_URL") {
                        parentItem.find(".image_wrapper_block a").attr("href", val);
                        parentItem.find(".dark_link").attr("href", val);
                    } 
                    if(i == "IMAGE") {
                        parentItem.find(".image_wrapper_block a img").attr("src", val);
                    }
                    if(i == "ARTICUL") parentItem.find(".article_block").html(val);
                    if(i == "PRICE") {
                        parentItem.find(".price_value").html(val);
                        parentItem.find(".price_currency").html("");
                    }
                    if(i == "ID") {
                        parentItem.find(".fast_view_block").attr("data-param-id",val);
                    }
                });
            } 
        });
    });
    // same fast view elements ajax
    $("body").on("click", ".same_elements.same_fast_view a", function(e){

        e.preventDefault();
        var parentItem = $("#fast_view_item");
        $.ajax({
            url: '/ajax/same_items.php',
            dataType : "json",
            data: {ID: $(this).attr("data-id"), action: 'sameChange', place: 'fastView'},
            success: function (data) {
                
                console.log(data);
                
                $.each(data, function(i, val) {
                    
                    if(i == "NAME") {
                        parentItem.find(".title a").html(val);
                        parentItem.find(".cheaper_form span").attr("data-autoload-product_name",val);
                        parentItem.find(".button_block .big_btn").attr("data-autoload-product_name",val);
                        parentItem.find(".preview_text").html(val);
                    }
                    if(i == "DETAIL_PAGE_URL") {
                        parentItem.find(".image_wrapper_block a").attr("href", val);
                        parentItem.find(".dark_link").attr("href", val);
                    } 
                    if(i == "IMAGE") {
                        parentItem.find(".item_slider .current img").attr("src", val);
                    }
                    if(i == "BIG_IMAGE") {
                        parentItem.find(".item_slider .current a").attr("href", val);
                    }
                    if(i == "ARTICUL") parentItem.find(".article .value").html(val);
                    if(i == "PRICE") {
                        parentItem.find(".price_value").html(val);
                        parentItem.find(".price_currency").html("");
                    }
                    if(i == "ID") {
                        parentItem.find(".button_block .big_btn").attr("data-autoload-product_id",val);
                        parentItem.find(".cheaper_form span").attr("data-autoload-product_id",val);
                    }
                    if(i == "CHARS") {
                        parentItem.find(".char_block .props_list").html(val);
                    }
                })
            } 
        });
    });
    // Ajax item change on item
    $("body").on("click", ".same_elements.same_detail a", function(e){
        e.preventDefault();

        var url = window.location.pathname;
        var urlParts = url.split('/');
        var urlLastID = urlParts.pop() || urlParts.pop();
        var newURL = url.replace(urlLastID, $(this).attr("data-id"));

        window.history.pushState("newURL", "", newURL);
        console.log(newURL);
        $.ajax({
            url: newURL,
            data: {ajax: "y"},
            success: function (data) {
                $(".right_block .middle .container .catalog_detail").html(data);                
            } 
        });

        var parentItem = $(this).closest(".item_main_info");
        $.ajax({
            url: '/ajax/same_items.php',
            dataType : "json",
            data: {ID: $(this).attr("data-id"), action: 'sameChange', place: 'item'},
            success: function (data) {
                console.log(data);
                
                $.each(data, function(i, val) {
                    if(i == "NAME") {
                        $("#pagetitle").html(val);
                    }
                });
            } 
        });
        

    })
})