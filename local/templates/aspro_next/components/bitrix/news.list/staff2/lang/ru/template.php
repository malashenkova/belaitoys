<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["PHONE"] = "Телефон";
$MESS["EMAIL"] = "E-mail";
$MESS["MOBILE_PHONE"] = "Мобильный телефон";
$MESS["SKYPE"] = "Skype";
$MESS["REGION"] = "Обслуживаемый регион";
$MESS["NO_STAFF"] = "Нет сотрудников";
$MESS["STAFF_OTHER_SECTION"] = "Прочие сотрудники";
?>