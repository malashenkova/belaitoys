<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!CModule::IncludeModule('redsign.devfunc'))
	return;

global $arrAccFilter;

// get no photo
$arResult['NO_PHOTO'] = RSDevFunc::GetNoPhoto(array('MAX_WIDTH' => 40, 'MAX_HEIGHT' => 40));
// /get no photo

$arResult['HAVE_PRODUCT_TYPE'] = array(
	'ITEMS' => false,
	'DELAYED' => false,
	'NOT_AVAILABLE' => false,
	'SUBSCRIBED' => false,
);

if ($_SESSION['CURRENCY'] != ''){
	foreach ($arResult['GRID']['ROWS'] as $key => $value) {
		$arResult['GRID']['ROWS'][$key]['PRICE'] = CCurrencyRates::ConvertCurrency($arResult['GRID']['ROWS'][$key]['PRICE'], "USD", $_SESSION['CURRENCY']);
		$arResult['GRID']['ROWS'][$key]['FULL_PRICE'] = CCurrencyRates::ConvertCurrency($arResult['GRID']['ROWS'][$key]['FULL_PRICE'], "USD", $_SESSION['CURRENCY']);
		$arResult['GRID']['ROWS'][$key]['PRICE_FORMATED'] = CurrencyFormat($arResult['GRID']['ROWS'][$key]['PRICE'], $_SESSION['CURRENCY']);
		$arResult['GRID']['ROWS'][$key]['FULL_PRICE_FORMATED'] = CurrencyFormat($arResult['GRID']['ROWS'][$key]['FULL_PRICE'], $_SESSION['CURRENCY']);
		$arResult['GRID']['ROWS'][$key]['SUM'] = CurrencyFormat($arResult['GRID']['ROWS'][$key]['PRICE']*$arResult['GRID']['ROWS'][$key]['QUANTITY'], $_SESSION['CURRENCY']);
	}
	$arResult['allSum'] = CCurrencyRates::ConvertCurrency($arResult['allSum'], 'USD', $_SESSION['CURRENCY']);
	$arResult['allSum_FORMATED'] = CurrencyFormat($arResult['allSum'], $_SESSION['CURRENCY']);
}else{
        foreach ($arResult['GRID']['ROWS'] as $key => $value) {
		$arResult['GRID']['ROWS'][$key]['PRICE'] = CCurrencyRates::ConvertCurrency($arResult['GRID']['ROWS'][$key]['PRICE'], "USD", "BYR");
		$arResult['GRID']['ROWS'][$key]['FULL_PRICE'] = CCurrencyRates::ConvertCurrency($arResult['GRID']['ROWS'][$key]['FULL_PRICE'], "USD", "BYR");
		$arResult['GRID']['ROWS'][$key]['PRICE_FORMATED'] = CurrencyFormat($arResult['GRID']['ROWS'][$key]['PRICE'], "BYR");
		$arResult['GRID']['ROWS'][$key]['FULL_PRICE_FORMATED'] = CurrencyFormat($arResult['GRID']['ROWS'][$key]['FULL_PRICE'],  "BYR");
		$arResult['GRID']['ROWS'][$key]['SUM'] = CurrencyFormat($arResult['GRID']['ROWS'][$key]['PRICE']*$arResult['GRID']['ROWS'][$key]['QUANTITY'],  "BYR");
	}
	$arResult['allSum'] = CCurrencyRates::ConvertCurrency($arResult['allSum'], 'USD',  "BYR");
	$arResult['allSum_FORMATED'] = CurrencyFormat($arResult['allSum'],  "BYR");
}


foreach($arResult['GRID']['ROWS'] as $k => $arItem)
{
	if($arItem['DELAY']=='N' && $arItem['CAN_BUY']=='Y')
	{
		$arResult['HAVE_PRODUCT_TYPE']['ITEMS'] = true;
	}
	
	if($arItem['DELAY']=='Y' && $arItem['CAN_BUY']=='Y')
	{
		$arResult['HAVE_PRODUCT_TYPE']['DELAYED'] = true;
	}
	
	if(isset($arItem['NOT_AVAILABLE']) && $arItem['NOT_AVAILABLE']==true)
	{
		$arResult['HAVE_PRODUCT_TYPE']['NOT_AVAILABLE'] = true;
	}
	
	if ($arItem['CAN_BUY']=='N' && $arItem['SUBSCRIBE']=='Y')
	{
		$arResult['HAVE_PRODUCT_TYPE']['SUBSCRIBED'] = true;
	}
}