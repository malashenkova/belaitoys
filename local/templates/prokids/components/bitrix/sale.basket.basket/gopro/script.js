var RSGoPro_BasketTimeoutID = 0;

$(document).ready(function(){
	
	$(document).on('click','.clearitems',function(){
		$(this).parents('.part').find('label').trigger('click');
		$(this).parents('form').find('.hiddensubmit').trigger('click');
		return false;
	});

	$(document).on('click','.clearsolo',function(){
		$(this).parents('form').find('.hiddensubmit').trigger('click');
		return false;
	});
	
	$(document).on('submit','#basket_form',function(){
		$('html').addClass('hidedefaultwaitwindow');
		RSGoPro_Area2Darken( $('#basket_form'), 'animashka' );
	});
	$(document).on('click','#basket_form a.delay, #basket_form a.delete, #basket_form a.add',function(){
		$('html').addClass('hidedefaultwaitwindow');
		setTimeout(function() {updateBaskets()}, 1000);
		RSGoPro_Area2Darken( $('#basket_form'), 'animashka' );
	});
	
	$(document).on('click','#basket_form .js-plus, #basket_form .js-minus',function(){
		var $link = $(this);
		clearTimeout(RSGoPro_BasketTimeoutID);
		RSGoPro_BasketTimeoutID = setTimeout(function(){
			$link.parents('form').find('.hiddensubmit').trigger('click');
		},1200);
	});
	
});

function updateBaskets () {
	$.ajax({
		type: 'POST',
		url: "/ajax/bottom_basket.php",
	}).done(function(response){
		$('#rs_easycart').find('#rsec_basket').html( response );
		setTimeout(function(){
			if( $('#rs_easycart').find('#rsec_basket').find('.rsec_take_normalCount').length>0 )
			{
				$('#rs_easycart').find('.rsec_normalCount').html( $('#rs_easycart').find('#rsec_basket').find('.rsec_take_normalCount').html() );
				$('#rs_easycart').find('.rsec_allSum_FORMATED').html( $('#rs_easycart').find('#rsec_basket').find('.rsec_take_allSum_FORMATED').html() );
			} else {
				$('#rs_easycart').find('.rsec_normalCount').html( '0' );
				$('#rs_easycart').find('.rsec_allSum_FORMATED').html( '0' );
			}
			$(document).trigger('RSEC_AfterBasketRefreshDone');

		},50);
	}).fail(function(){
		console.warn( 'RSEasyCart -> Basket -> error' );
		$(document).trigger('RSEC_AfterBasketRefreshError');
	}).always(function(){
		RSEC_UnBlockTab();
		$(document).trigger('RSEC_AfterBasketRefresh');
	});

	$.ajax({
		type: 'POST',
		url: "/ajax/top_basket.php",
	}).done(function(response){
		$('#topBasket .column1inner').html( response );
	}).fail(function(){
		console.warn( 'RSEasyCart -> Basket -> error' );
	});

	return false;
}