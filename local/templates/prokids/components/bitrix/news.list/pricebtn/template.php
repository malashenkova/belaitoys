<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);

?>
<style>

.price_pdf{
position:relative;
}
#block_price_site{
display:none;
position:absolute;
right:40px;
top:75px;
z-index:999;
width:153px;
background:#fff;
border:1px solid #e6e6e6;
padding:10px;
box-shadow: 0.4em 0.4em 5px rgba(122,122,122,0.5);

}
.pngicons2 {
    background-image: url(img/icons.png?v100);
}

.fancybox-close .icon {
    width: 10px;
    height: 10px;
    background-position: 0px -91px;
}

#block_price_site::before, #block_price_site::after {
    content: ''; 
    position: absolute;
    left: 20px; top: -20px;
    border: 10px solid transparent;
    border-top: 10px solid #e6e6e6;
   border: 10px solid transparent;	
border-bottom: 10px solid green;
   }
   #block_price_site::after {
    border-bottom: 10px solid white;
    top: -20px; 
   }


#price_pdf_open:hover{
 background:#fa6900;
}
.mylink:hover{
 background:#fa6900;
}
.time_work{
position:relative;
}
.mylink{
display:block;
  margin-top:5px;
 background: #f38630;
    font-size: 13px!important;
    width: 100%;   
    border-radius: 7px;
    color: #fff!important;
font-family: Opensansbold,Arial,Helvetica,sans-serif;
}
#block_price_site li {
    list-style-type: none; /* Убираем маркеры */
 width: 100%;
 
   }
#block_price_site ul{
margin:0;
padding:0!important;
}

#block_price_site_close{
   position:absolute;
right:10px;
width:11px;
height:12px;
cursor:pointer;
}

#block_price_site ul{
  margin-top:30px;
}
#block_price_site_close {
    background-image: url(/bitrix/templates/prokids/img/close.png)!important;
}
</style>
<div id="block_price_site" >
    <div id="block_price_site_close" ></div>
    <ul>
 
<? if(count($arResult["ITEMS"])>0){

  foreach($arResult["ITEMS"]  as  $element){?>
           <?if(!empty($element["PROPERTIES"]["PRICE"]["VALUE"])){?>
           <li><a class="mylink" target="blank" href="<?=CFile::GetPath($element["PROPERTIES"]["PRICE"]["VALUE"]);?>"><?=$element["NAME"]?></a></li>
  <?
}
 }?>
     </ul>
</div>
<?}?>

<? //echo"<pre>"; print_r( $arResult["ITEMS"]  ); echo "</pre>"; ?>
