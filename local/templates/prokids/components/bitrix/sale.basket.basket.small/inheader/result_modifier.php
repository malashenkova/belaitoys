<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

if(!CModule::IncludeModule('redsign.devfunc'))
	return;

$arResult = RSDevFuncResultModifier::SaleBasketBasketSmall($arResult);
$arResult["RIGHT_WORD"] = RSDevFunc::BasketEndWord($arResult["NUM_PRODUCTS"]);

if ($_SESSION['CURRENCY'] != ''){
	$arResult['FULL_PRICE'] = CCurrencyRates::ConvertCurrency($arResult['FULL_PRICE'], "USD", $_SESSION['CURRENCY']);
	$arResult['PRINT_FULL_PRICE'] = CurrencyFormat($arResult['FULL_PRICE'], $_SESSION['CURRENCY']);
}else{
	$arResult['FULL_PRICE'] = CCurrencyRates::ConvertCurrency($arResult['FULL_PRICE'], "USD", "BYR");
	$arResult['PRINT_FULL_PRICE'] = CurrencyFormat($arResult['FULL_PRICE'], "BYR");
}
