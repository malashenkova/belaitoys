<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
 if ($_SESSION['CURRENCY'] != ''){
 	$APPLICATION->RestartBuffer();
 	$url=explode('?',$arResult["FILTER_URL"]);
 	$url2=explode('&',$url[1]);
 	foreach ($url2 as $k=>$u){
 	   $url3=explode('=',$u);
		if ($url3[0]=='arrFilter_P1_MAX'){
			$url2[$k]='arrFilter_P1_MAX='.CCurrencyRates::ConvertCurrency($url3[1], $_SESSION['CURRENCY'], "USD");
		}
		if ($url3[0]=='arrFilter_P1_MIN'){
			$url2[$k]='arrFilter_P1_MIN='.CCurrencyRates::ConvertCurrency($url3[1], $_SESSION['CURRENCY'],"USD");
		}
	}
	$arResult["FILTER_URL"]=$url[0].'?'.implode('&',$url2);
	unset($arResult["COMBO"]);
	echo CUtil::PHPToJSObject($arResult);
 }else{
	$APPLICATION->RestartBuffer();
	unset($arResult["COMBO"]);
	echo CUtil::PHPToJSObject($arResult, true);
 }
?>