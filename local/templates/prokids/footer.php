<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
IncludeTemplateLangFile(__FILE__);
				?></div><?
			?></div><?
		?></div><!-- /content --><?
	?></div><!-- /body --><?
	// scripts
	?><script type="text/javascript">RSGoPro_SetSet();</script><?
	?><div id="footer" class="footer"><!-- footer --><?
		?><div class="centering"><?
			?><div class="centeringin line1 clearfix"><?
				?><div class="block one"><?
					?><div class="logo"><?
						?><a href="<?=SITE_DIR?>"><?
							$APPLICATION->IncludeFile(
								SITE_TEMPLATE_PATH."/include_areas/footer_logo.php",
								Array(),
								Array("MODE"=>"html")
							);
						?></a><?
					?></div><?
					?><div class="contacts clearfix"><?
						?><div class="phone1"><?/*?><a class="fancyajax fancybox.ajax recall" href="/recall/" title="<?=GetMessage('RSGOPRO.RECALL')?>"><i class="icon pngicons"></i><?=GetMessage('RSGOPRO.RECALL')?></a><?
							*/?><div class="phone"><?
								$APPLICATION->IncludeFile(
									SITE_TEMPLATE_PATH."/include_areas/footer_phone1.php",
									Array(),
									Array("MODE"=>"html")
								);
							?></div><?
						?></div><?
						?><div class="phone2"><?
							?><a class="fancyajax fancybox.ajax feedback" href="/feedback/" title="<?=GetMessage('RSGOPRO.FEEDBACK')?>"><?=GetMessage('RSGOPRO.FEEDBACK')?></a><?
							/*?><div class="phone"><?
								$APPLICATION->IncludeFile(
									SITE_TEMPLATE_PATH."/include_areas/footer_phone2.php",
									Array(),
									Array("MODE"=>"html")
								);
							?></div><?*/
						?></div><?
					?></div><?
				?></div><?
				?><div class="block two"><?
				$APPLICATION->IncludeComponent(
					"bitrix:menu",
					"infootercatalog",
					array(
						"ROOT_MENU_TYPE" => "footercatalog",
						"MENU_CACHE_TYPE" => "A",
						"MENU_CACHE_TIME" => "3600",
						"MENU_CACHE_USE_GROUPS" => "Y",
						"MENU_CACHE_GET_VARS" => array(
						),
						"MAX_LEVEL" => "1",
						"CHILD_MENU_TYPE" => "",
						"USE_EXT" => "Y",
						"DELAY" => "N",
						"ALLOW_MULTI_SELECT" => "N",
						"BLOCK_TITLE" => "Товары",
						"LVL1_COUNT" => "6",
						"LVL2_COUNT" => "4",
						"ELLIPSIS_NAMES" => "Y"
					),
					false
				);
				?></div><?
				?><?
				?><div class="block four"><?
					?><div class="sovservice"><?
						$APPLICATION->IncludeFile(
							SITE_TEMPLATE_PATH."/include_areas/footer_socservice.php",
							Array(),
							Array("MODE"=>"html")
						);
					?></div><?
					?><div class="subscribe"><?
						$APPLICATION->IncludeComponent(
							"bitrix:subscribe.form",
							"footer",
							array(
								"USE_PERSONALIZATION" => "Y",
								"SHOW_HIDDEN" => "N",
								"PAGE" => "/personal/subscribe/",
								"CACHE_TYPE" => "A",
								"CACHE_TIME" => "36000000",
							),
							false
						);
					?></div><?
				?></div><?
			?></div><?
		?></div><?

		?><div class="line2"><?
			?><div class="centering"><?
				?><div class="centeringin clearfix"><?
					?><div class="sitecopy"><?
						$APPLICATION->IncludeFile(
							SITE_TEMPLATE_PATH."/include_areas/footer_sitecopy.php",
							Array(),
							Array("MODE"=>"html")
						);
					?></div><?
					?><div class="developercopy"><?
						/****************************************************************************************/
						/* Удаление ссылки на разработчика является грубым нарушением лицензионного соглашения и может являться причиной отказа в оказании технической поддержки. */
						/****************************************************************************************/
						/*?>Powered by <a href="http://redsign.ru/" target="_blank">ALFA Systems</a><?*/
						?>
  Доработка, продвижение <a href="https://megarost.by/" target="_blank">"Мегарост групп"</a> 





<?
					?></div><?
				?></div><?
			?></div><?
		?></div><?
	?></div><!-- /footer --><?
	?><div id="fixedcomparelist"><?
		$APPLICATION->IncludeComponent(
			"bitrix:catalog.compare.list",
			"session",
			array(
				"IBLOCK_TYPE" => "catalog",
				"IBLOCK_ID" => "49",
				"NAME" => "CATALOG_COMPARE_LIST",
			),
			false
		);
	?></div>



<?
	$APPLICATION->IncludeComponent(
	"redsign:easycart", 
	"gopro", 
	array(
		"USE_VIEWED" => "Y",
		"USE_COMPARE" => "N",
		"USE_BASKET" => "Y",
		"USE_FAVORITE" => "Y",
		"VIEWED_COUNT" => "10",
		"FAVORITE_COUNT" => "10",
		"COMPARE_NAME" => "CATALOG_COMPARE_LIST",
		"TEMPLATE_THEME" => "yellow",
		"Z_INDEX" => "991",
		"MAX_WIDTH" => "1240",
		"USE_ONLINE_CONSUL" => "N",
		"ONLINE_CONSUL_LINK" => "#",
		"INCLUDE_JQUERY" => "N",
		"INCLUDE_JQUERY_COOKIE" => "N",
		"INCLUDE_JQUERY_STICKY" => "N",
		"ADD_BODY_PADDING" => "Y",
		"ON_UNIVERSAL_AJAX_HANDLER" => "Y",
		"UNIVERSAL_AJAX_FINDER" => "action=ADD2BASKET",
		"COMPARE_IBLOCK_TYPE" => "catalog",
		"COMPARE_IBLOCK_ID" => "49",
		"COMPARE_RESULT_PATH" => "/catalog/compare/",
		"UNIVERSAL_AJAX_FINDER_COMPARE" => "action=ADD_TO_COMPARE_LIST",
		"UNIVERSAL_AJAX_FINDER_BASKET" => "action=ADD2BASKET",
		"UNIVERSAL_AJAX_FINDER_FAVORITE" => "action=add2favorite",
		"UNIVERSAL_AJAX_FINDER_COMPARE_ADD" => "action=ADD_TO_COMPARE_LIST",
		"UNIVERSAL_AJAX_FINDER_COMPARE_REMOVE" => "action=DELETE_FROM_COMPARE_LIST",
		"COMPONENT_TEMPLATE" => "gopro"
	),
	false
);
	?><div style="display:none;">AlfaSystems GoPro GP261D21</div><?

?>

<script> 
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ 
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), 
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) 
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga'); 

ga('create', 'UA-104209581-1', 'auto'); 
ga('send', 'pageview'); 

</script>
<script type="text/javascript" > 
(function (d, w, c) { 
(w[c] = w[c] || []).push(function() { 
try { 
w.yaCounter45558177 = new Ya.Metrika({ 
id:45558177, 
clickmap:true, 
trackLinks:true, 
accurateTrackBounce:true, 
webvisor:true 
}); 
} catch(e) { } 
}); 

var n = d.getElementsByTagName("script")[0], 
s = d.createElement("script"), 
f = function () { n.parentNode.insertBefore(s, n); }; 
s.type = "text/javascript"; 
s.async = true; 
s.src = "https://mc.yandex.ru/metrika/watch.js"; 

if (w.opera == "[object Opera]") { 
d.addEventListener("DOMContentLoaded", f, false); 
} else { f(); } 
})(document, window, "yandex_metrika_callbacks"); 
</script> 
<noscript><div><img src="https://mc.yandex.ru/watch/45558177" style="position:absolute; left:-9999px;" alt="" /></div></noscript> 
<script>
        (function(w,d,u){
                var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
                var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
        })(window,document,'https://cdn-ru.bitrix24.ru/b16328248/crm/site_button/loader_5_qzmglc.js');
</script>
</body>

<?
?></html>