var RSGoPro_AJAXPAGES_processing = !1;

function RSGoPro_PutJSon(c, a, b) {
    if ("OK" == c.TYPE) {
        b && b == c.IDENTIFIER ? (c.HTML.catalognames && $("#" + b).find(".names > tbody > tr:last").after(c.HTML.catalognames), c.HTML.catalogproducts && $("#" + b).find(".products > tbody > tr:last").after(c.HTML.catalogproducts), c.HTML.showcaseview && $("#" + b).find("#showcaseview").append(c.HTML.showcaseview), a && c.HTML.catalogajaxpages ? a.parents(".ajaxpages").replaceWith(c.HTML.catalogajaxpages) : a && a.parents(".ajaxpages").remove()) : console.warn("PutJSon -> no ajaxpages");
        if (c.HTMLBYID) for (var d in c.HTMLBYID) $("#" +
            d) && $("#" + d).html(c.HTMLBYID[d]);
        var e = c.HTMLBYID.basketinfo, f = e.indexOf("$") + 1;
        c = e.substr(f);
        $.ajax({
            url: "/ajax/convert.php", type: "POST", data: {suma: c}, beforeSend: function () {
            }, success: function (a) {
                a = $.parseJSON(a);
                "USD" != a.CURRENCY && $("#basketinfo").text(e.substr(0, f - 1) + a.SUMM)
            }
        })
    } else console.warn("PutJSon -> request return error")
}

function RSGoPro_AjaxPages(c) {
    if (c.parent().hasClass("animation")) {
        c.parent().removeClass("animation");
        var a = $("#" + c.data("ajaxpagesid"));
        if (0 < a.length && 0 < a.find(".artables").length && 1 < a.find(".artables .names > tbody > tr").length) {
            var b = 0;
            a.find(".artables .names > tbody > tr").each(function (d) {
                b = $(this).data("elementid");
                0 == d % 2 ? ($(this).addClass("even"), a.find(".artables .products tr.js-elementid" + b).addClass("even")) : ($(this).removeClass("even"), a.find(".artables .products tr.js-elementid" + b).removeClass("even"))
            })
        }
    } else c.parent().addClass("animation")
}

function RSGoPro_Area2Darken(c, a, b) {
    b = $.extend({progressLeft: !1, progressTop: !1}, b);
    c.hasClass("areadarken") ? c.removeClass("areadarken").removeAttr("style").find(".area2darken").remove() : (c.addClass("areadarken").css({position: "relative"}).append('<div class="area2darken"></div>'), "animashka" == a && (c.find(".area2darken").append('<i class="icon animashka"></i>'), b.progressTop && c.find(".animashka").css({top: b.progressTop})))
}

function RSGoPro_SetSet() {
    RSGoPro_SetFavorite();
    RSGoPro_SetCompared();
    RSGoPro_SetInBasket()
}

function RSGoPro_SetFavorite() {
    $(".add2favorite").removeClass("in");
    for (element_id in RSGoPro_FAVORITE) "Y" == RSGoPro_FAVORITE[element_id] && 0 < $(".js-elementid" + element_id).find(".add2favorite").length && $(".js-elementid" + element_id).find(".add2favorite").addClass("in")
}

function RSGoPro_SetCompared() {
    $(".add2compare").removeClass("in").html('<i class="icon pngicons"></i>' + BX.message("RSGOPRO_JS_COMPARE"));
    for (element_id in RSGoPro_COMPARE) "Y" == RSGoPro_COMPARE[element_id] && 0 < $(".js-elementid" + element_id).find(".add2compare").length && $(".js-elementid" + element_id).find(".add2compare").addClass("in").html('<i class="icon pngicons"></i>' + BX.message("RSGOPRO_JS_COMPARE_IN"))
}

function RSGoPro_SetInBasket() {
    $(".add2basketform").removeClass("in");
    for (element_id in RSGoPro_INBASKET) "Y" == RSGoPro_INBASKET[element_id] && 0 < $(".js-add2basketpid[value='" + element_id + "']").length && $('.js-add2basketpid[value="' + element_id + '"]').parents(".add2basketform").addClass("in"), 0 < parseInt(RSGoPro_INBASKET[element_id]) && 0 < $(".products").find(".js-add2basketform" + RSGoPro_INBASKET[element_id]).length && $(".products").find(".js-add2basketform" + RSGoPro_INBASKET[element_id]).addClass("in")
}

function RSGoPro_AJAXPAGESAuto() {
    $(".ajaxpages.auto").each(function (c) {
        c = $(this);
        var a = c.offset().top, b = $(window).height();
        200 > a - window.pageYOffset - b && !RSGoPro_AJAXPAGES_processing && !c.hasClass("") && c.find("a").trigger("click")
    })
}

function RSGoPro_TIMER() {
    var c = new Date;
    c = Date.parse(c) / 1E3;
    $(".timer").each(function (a) {
        timetimer1 = $(this).find(".intimer");
        a = $(this).data("datefrom");
        var b = $(timetimer1).data("dateto"), d = $(timetimer1).data("autoreuse"), e = b - a;
        a = b - c;
        if (0 > a && "Y" == d) for (lim = 0; 100 > lim; lim++) if (newdateTo = lim * e + b - c, 0 < newdateTo) {
            a = newdateTo;
            break
        }
        0 < a ? (b = parseInt(a / 3600 / 24), 10 > b && (b = "0" + b), b = b.toString(), d = parseInt(a / 3600 % 24), 10 > d && (d = "0" + d), d = d.toString(), e = parseInt(a / 60) % 60, 10 > e && (e = "0" + e), e = e.toString(), a = parseInt(a) %
            60, 10 > a && (a = "0" + a), a = a.toString(), $(timetimer1).find(".result-day").text(b), $(timetimer1).find(".result-hour").text(d), $(timetimer1).find(".result-minute").text(e), $(timetimer1).find(".result-second").text(a)) : ($(timetimer1).parents(".timer").css("display", "none"), $(timetimer1).parents(".js-element").removeClass("da2 qb").find(".price").removeClass("new"))
    })
}

function RSGoPro_InitMaskPhone() {
    0 < $(".maskPhone").length && $(".maskPhone").mask("+7 (999) 999-9999")
}

$(document).ready(function () {


/*btn price*/
$( "#price_pdf_open" ).click(function() {
      $("#block_price_site").css("display","block");
});
$( "#block_price_site_close" ).click(function() {
      $("#block_price_site").css("display","none");
});
$(window).resize(function() {
    $("#block_price_site").css("display","none");
});



    setInterval(function () {
        RSGoPro_TIMER()
    }, 1E3);
    $(document).on("RSGoProOnOfferChange", function (a, b) {
        if (0 < $(b).find(".timers").length && "N" == $(b).find(".intimer").data("autoreuse")) {
            var d = new Date;
            d = Date.parse(d) / 1E3;
            $(b).find(".timer").data("datefrom");
            0 > $(b).find(".intimer").data("dateto") - d && ($(b).find(".timers").css("display", "none"), $(b).removeClass("da2 qb"), $(b).find(".price").removeClass("new"))
        }
    });
    $(document).on("click", ".click_protection", function (a) {
        a.stopImmediatePropagation();
        console.warn("Click protection");
        return !1
    });
    $(document).on("click", "form a.submit", function () {
        $(this).parents("form").find('input[type="submit"]').trigger("click");
        return !1
    });
    $(document).on("submit", ".add2basketform", function () {
        var a = $(this), b = parseInt(a.find(".js-add2basketpid").val());
        if (0 < b) {
            var d = $(this).serialize();
            d = SITE_DIR + SITE_CATALOG_PATH + "/?" + d + "&AJAX_CALL=Y&action=add2basket";
            RSGoPro_Area2Darken(a);
            RSGoPro_Area2Darken($("#header").find(".basketinhead"));
            $.getJSON(d, {}, function (a) {
                "OK" ==
                a.TYPE ? (RSGoPro_INBASKET[b] = "Y", RSGoPro_SetInBasket(), RSGoPro_PutJSon(a)) : console.warn("add2basket - error responsed")
            }).fail(function (a) {
                console.warn("add2basket - can't load json")
            }).always(function () {
                RSGoPro_Area2Darken(a);
                RSGoPro_Area2Darken($("#header").find(".basketinhead"))
            })
        } else 1 > a.parents(".elementpopup").length ? RSDevFunc_PHONETABLET ? 0 < a.parents(".js-element").find(".js-detaillink").length ? window.location = "http://" + window.location.hostname + a.parents(".js-element").find(".js-detaillink").attr("href") :
            console.warn("fail redirect - can't find link") : RSGoPro_GoPopup(a.parents(".js-element").data("elementid"), a.parents(".js-element")) : console.warn("add product to basket failed");
        return !1
    });
    $(document).on("click", ".add2compare", function () {
        var a = $(this), b = parseInt(a.parents(".js-element").data("elementid")), d = "";
        0 < b && (RSGoPro_Area2Darken($(".add2compare")), d = "Y" == RSGoPro_COMPARE[b] ? "DELETE_FROM_COMPARE_LIST" : "ADD_TO_COMPARE_LIST", $.getJSON(SITE_DIR + SITE_CATALOG_PATH + "/?AJAX_CALL=Y&action=" + d + "&id=" +
            b, {}, function (a) {
            "OK" == a.TYPE ? (RSGoPro_PutJSon(a), "DELETE_FROM_COMPARE_LIST" == d ? delete RSGoPro_COMPARE[b] : RSGoPro_COMPARE[b] = "Y") : console.warn("compare - error responsed")
        }).fail(function (a) {
            console.warn("compare - fail request")
        }).always(function () {
            RSGoPro_Area2Darken($(".add2compare"));
            RSGoPro_SetCompared()
        }));
        return !1
    });
    $(document).on("click", ".add2favorite", function () {
        var a = $(this), b = parseInt(a.parents(".js-element").data("elementid"));
        0 < b && (RSGoPro_Area2Darken($(".add2favorite")), $.getJSON(SITE_DIR +
            SITE_CATALOG_PATH + "/?AJAX_CALL=Y&action=add2favorite&element_id=" + b, {}, function (a) {
            "OK" == a.TYPE ? (RSGoPro_PutJSon(a), "Y" == RSGoPro_FAVORITE[b] ? delete RSGoPro_FAVORITE[b] : RSGoPro_FAVORITE[b] = "Y") : console.warn("favorite - error responsed")
        }).fail(function (a) {
            console.warn("favorite - fail request")
        }).always(function () {
            RSGoPro_Area2Darken($(".add2favorite"));
            RSGoPro_SetFavorite()
        }));
        return !1
    });
    $(document).on("click", ".ajaxpages a", function () {
        var a = $(this), b = a.data("ajaxurl"), c = a.data("ajaxpagesid"), e = a.data("navpagenomer"),
            f = a.data("navpagecount"), g = a.data("navnum"), h = parseInt(e) + 1, k = "";
        0 < $("#" + c).length && e < f && 0 < parseInt(g) && "" != b ? (RSGoPro_AJAXPAGES_processing = !0, RSGoPro_AjaxPages(a), k = 1 > b.indexOf("?") ? b + "?ajaxpages=Y&ajaxpagesid=" + c + "&PAGEN_" + g + "=" + h : b + "&ajaxpages=Y&ajaxpagesid=" + c + "&PAGEN_" + g + "=" + h, $.getJSON(k, {}, function (b) {
            RSGoPro_PutJSon(b, a, c)
        }).fail(function (a) {
            console.warn("ajaxpages - error responsed")
        }).always(function () {
            setTimeout(function () {
                RSGoPro_AJAXPAGES_processing = !1;
                RSGoPro_AjaxPages(a)
            }, 50)
        })) : (0 <
        $("#" + c).length || console.warn("AJAXPAGES: ajaxpages -> empty DOM element"), e < f || console.warn("AJAXPAGES: ajaxpages -> navpagenomer !< navpagecount"), 0 < parseInt(g) || console.warn("AJAXPAGES: ajaxpages -> parseInt(navnum)!>0"), "" == b && console.warn("AJAXPAGES: ajaxpages -> ajaxurl is empty"));
        return !1
    });
    $(window).scroll(function () {
        RSGoPro_AJAXPAGESAuto()
    });
    $(document).on("click", ".prices .arrowtop", function () {
        var a = $(this);
        3 < a.parent().find("tr").length && !$(this).parent().find("tr:first").is(":visible") &&
        a.parent().find("tr").each(function (b) {
            if (!$(this).hasClass("noned")) return a.parent().find("tr:eq(" + (b - 1) + ")").removeClass("noned"), a.parent().find("tr:eq(" + (b + 2) + ")").addClass("noned"), !1
        });
        return !1
    });
    $(document).on("click", ".prices .arrowbot", function () {
        var a = $(this);
        3 < a.parent().find("tr").length && !$(this).parent().find("tr:last").is(":visible") && a.parent().find("tr").each(function (b) {
            if (!$(this).hasClass("noned")) return $(this).addClass("noned"), a.parent().find("tr:eq(" + (b + 3) + ")").removeClass("noned"),
                !1
        });
        return !1
    });
    $(document).on("mouseenter mouseleave", ".prices .arrowtop, .prices .arrowbot, .js-minus, .js-plus", function () {
        $("html").toggleClass("disableSelection")
    });
    $(document).on("click", ".js-minus", function () {
        var a = $(this), b = parseFloat(a.parent().find(".js-quantity").data("ratio")),
            c = b.toString().split(".", 2)[1], e = 0;
        void 0 !== c && (e = c.length);
        c = parseFloat(a.parent().find(".js-quantity").val());
        c > b && a.parent().find(".js-quantity").val((c - b).toFixed(e));
        return !1
    });
    $(document).on("click", ".js-plus",
        function () {
            var a = $(this), b = parseFloat(a.parent().find(".js-quantity").data("ratio")),
                c = b.toString().split(".", 2)[1], e = 0;
            void 0 !== c && (e = c.length);
            c = parseFloat(a.parent().find(".js-quantity").val());
            a.parent().find(".js-quantity").val((c + b).toFixed(e));
            return !1
        });
    $(document).on("blur", ".js-quantity", function () {
        var a = $(this), b = parseFloat(a.data("ratio")), c = b.toString().split(".", 2)[1], e = 0;
        void 0 !== c && (e = c.length);
        c = parseFloat(a.val());
        0 < c ? a.val((b * Math.floor(c / b)).toFixed(e)) : a.val(b)
    });
    var c = {};
    c = {};
    c = {
        maxWidth: 900,
        maxHeight: 600,
        minWidth: 250,
        minHeight: 100,
        fitToView: !0,
        autoSize: !0,
        openEffect: "none",
        closeEffect: "none",
        padding: 20,
        tpl: {closeBtn: '<a title="Close" class="fancybox-item fancybox-close" href="javascript:;"><i class="icon pngicons"></i></a>'},
        helpers: {title: {type: "inside", position: "top"}},
        beforeLoad: function () {
            RSGoPro_HideAllPopup()
        },
        beforeShow: function () {
            $(".fancybox-wrap").css({marginLeft: "-10000px"});
            $(document).trigger("RSGoProOnFancyBeforeShow")
        },
        afterShow: function () {
            setTimeout(function () {
                    $.fancybox.toggle()
                },
                50);
            setTimeout(function () {
                $(".fancybox-wrap").css({marginLeft: "0px"});
                RSGoPro_InitMaskPhone()
            }, 75)
        }
    };
    $(".fancyajax:not(.big)").fancybox(c);
    c = $.extend({}, c);
    c.width = "80%";
    c.height = "80%";
    c.autoSize = !1;
    c.autoHeight = !0;
    $(".fancyajax.big").fancybox(c);
    RSGoPro_InitMaskPhone();
    $(document).on("focus blur", ".dropdown-block .bx-ui-sls-fake", function () {
        $(this).parents(".dropdown-block").toggleClass("focus")
    })
});
!function () {
    function c() {
        var a = $(".currency");
        if (a.length) a.find(".icon").on("click", function () {
            a.toggleClass("open");
            $(document).click(function (b) {
                $(b.target).closest(a).length || (a.hasClass("open") && a.removeClass("open"), b.stopPropagation())
            })
        })
    }

    $(window).load(function () {
        c()
    })
}();