var RSGoPro_OffersExt_timeout_id = 0;

function formatnum(a) {
    var b = "", f = 0;
    for (i = a.length - 1; 0 <= i; i--) 3 > f ? (f++, b = a.charAt(i) + b) : (f = 1, b = a.charAt(i) + " " + b);
    return b
}

function RSGoPro_OffersExt_ChangeHTML(a) {
    var b = a.data("elementid");
    if (RSGoPro_OFFERS[b]) {
        var f = {};
        a.find(".div_option.selected").each(function (a) {
            var b = $(this);
            a = b.parents(".offer_prop").data("code");
            b = b.data("value");
            f[a] = b
        });
        var c = 0, d = !0;
        for (offerID in RSGoPro_OFFERS[b].OFFERS) {
            d = !0;
            for (pCode in f) if (f[pCode] != RSGoPro_OFFERS[b].OFFERS[offerID].PROPERTIES[pCode]) {
                d = !1;
                break
            }
            if (d) {
                c = offerID;
                break
            }
        }
       // console.log(RSGoPro_OFFERS[b].OFFERS[c].CAN_BUY);

        a.find(".can_buy_style").html(RSGoPro_OFFERS[b].OFFERS[c].CAN_BUY? "Наличие: Есть в наличии":"Наличие: Нет в наличии")

        a.find(".offer_article") && (RSGoPro_OFFERS[b].OFFERS[c].ARTICLE ? a.find(".offer_article").html(RSGoPro_OFFERS[b].OFFERS[c].ARTICLE).parent().css("visibility",
            "visible") : a.find(".offer_article").data("prodarticle") ? a.find(".offer_article").html(a.find(".offer_article").data("prodarticle")).parent().css("visibility", "visible") : a.find(".offer_article").parent().css("visibility", "hidden"));
        if (RSGoPro_OFFERS[b].OFFERS[c].PRICES) for (e in d = RSGoPro_OFFERS[b].OFFERS[c].PRICES, d) {
            if ("Optovaya" == e && 0 < parseInt(a.find(".optPrice").attr("data-discount")) && 0 < parseInt(d[e].PRINT_DISCOUNT_VALUE)) {
                var g = parseInt(d[e].PRINT_DISCOUNT_VALUE.split(" ").join("")),
                    h = d[e].PRINT_DISCOUNT_VALUE.split(" ").join("").replace(g,
                        "");
                g = Math.round(g * (100 - parseInt(a.find(".optPrice").attr("data-discount"))) / 100) + "";
                3 < g.length && (g = formatnum(g));
                g = g + " " + h;
                a.find(".elemDiscontPrice").html(g)
            }
            a.find(".price_pdv_" + e) && (a.find(".price_pdv_" + e).removeClass("new").html(d[e].PRINT_DISCOUNT_VALUE), 0 < parseInt(d[e].DISCOUNT_DIFF) && a.find(".price_pdv_" + e).addClass("new"));
            a.find(".price_pd_" + e) && (0 < parseInt(d[e].DISCOUNT_DIFF) ? a.find(".price_pd_" + e).html(d[e].PRINT_DISCOUNT) : a.find(".price_pd_" + e).html(""));
            a.find(".price_pv_" + e) && (0 < parseInt(d[e].DISCOUNT_DIFF) ?
                a.find(".price_pv_" + e).html(d[e].PRINT_VALUE) : a.find(".price_pv_" + e).html(""))
        }
        RSGoPro_OFFERS[b].OFFERS[c].CATALOG_MEASURE_RATIO && a.find(".js-quantity") && (a.find(".js-quantity").data("ratio", RSGoPro_OFFERS[b].OFFERS[c].CATALOG_MEASURE_RATIO), a.find(".js-quantity").val(a.find(".js-quantity").data("ratio")));
        RSGoPro_OFFERS[b].OFFERS[c].CATALOG_MEASURE_NAME && a.find(".js-measurename") && a.find(".js-measurename").html(RSGoPro_OFFERS[b].OFFERS[c].CATALOG_MEASURE_NAME);
        a.removeClass("qb da2");
        a.find(".timers .timer").hide();
        if (RSGoPro_OFFERS[b].ELEMENT.QUICKBUY || RSGoPro_OFFERS[b].OFFERS[c].QUICKBUY) a.addClass("qb"), 0 < a.find(".timers .qb.js-timer_id" + b).length ? a.find(".timers .qb.js-timer_id" + b).css("display", "inline-block") : 0 < a.find(".timers .qb.js-timer_id" + c).length && a.find(".timers .qb.js-timer_id" + c).css("display", "inline-block");
        if (RSGoPro_OFFERS[b].ELEMENT.DAYSARTICLE2 || RSGoPro_OFFERS[b].OFFERS[c].DAYSARTICLE2) a.removeClass("qb").addClass("da2"), 0 < a.find(".timers .da2.js-timer_id" + b).length ? (a.find(".timers .timer").hide(),
            a.find(".timers .da2.js-timer_id" + b).css("display", "inline-block")) : 0 < a.find(".timers .da2.js-timer_id" + c).length && (a.find(".timers .timer").hide(), a.find(".timers .da2.js-timer_id" + c).css("display", "inline-block"));
        a.find(".js-add2basketpid").val(c);
        RSGoPro_OFFERS[b].OFFERS[c].CAN_BUY ? a.find(".add2basketform").removeClass("cantbuy") : a.find(".add2basketform").addClass("cantbuy");
        if (a.find(".stores") && 0 < c && a.find(".stores").find(".popupstores")) if (RSGoPro_STOCK[b]) {
            if (a.find(".stores").hasClass("gopro_20")) for (storeID in RSGoPro_STOCK[b].JS.SKU[c]) {
                var e =
                    RSGoPro_STOCK[b].JS.SKU[c][storeID];
                1 == RSGoPro_STOCK[b].USE_MIN_AMOUNT ? 1 > e ? a.find(".stores").find(".store_" + storeID).find(".amount").css("color", "#ff0000").html(RSGoPro_STOCK[b].MESSAGE_EMPTY) : e < RSGoPro_STOCK[b].MIN_AMOUNT ? a.find(".stores").find(".store_" + storeID).find(".amount").css("color", "").html(RSGoPro_STOCK[b].MESSAGE_LOW) : a.find(".stores").find(".store_" + storeID).find(".amount").css("color", "#00cc00").html(RSGoPro_STOCK[b].MESSAGE_ISSET) : a.find(".stores").find(".store_" + storeID).find(".amount").html(e);
                0 == RSGoPro_STOCK[b].SHOW_EMPTY_STORE && 1 > e ? a.find(".stores").find(".store_" + storeID).hide() : a.find(".stores").find(".store_" + storeID).show()
            } else console.warn("OffersExt_ChangeHTML -> old stores template version"), a.find(".stores").find(".genamount").removeClass("cantopen"), a.find(".stores").find(".popupstores").find(".offerstore").hide(), 0 < a.find(".stores").find(".popupstores").find(".offer_" + c).length ? a.find(".stores").find(".popupstores").find(".offer_" + c).show() : a.find(".stores").find(".genamount").addClass("cantopen");
            RSGoPro_STOCK[b].QUANTITY[b] && (e = parseInt(RSGoPro_STOCK[b].QUANTITY[c]))
        } else console.warn("OffersExt_ChangeHTML -> store not found");
        RSGoPro_SetInBasket();
        $(document).trigger("RSGoProOnOfferChange", [a])
    }
}

function RSGoPro_OffersExt_PropChanged(a) {
    var b = a.parents(".js-element").data("elementid"), f = a.parents(".offer_prop").data("code"), c = a.data("value");
    if (RSGoPro_OFFERS[b] && !a.hasClass("disabled")) {
        a.parents(".offer_prop").removeClass("opened").addClass("closed");
        a.parents(".offer_prop").find(".div_option").removeClass("selected");
        a.addClass("selected");
        a.parents(".offer_prop").hasClass("color") ? a.parents(".offer_prop").find(".div_selected span").css({backgroundImage: a.find("span").css("backgroundImage")}) :
            a.parents(".offer_prop").find(".div_selected span").html(c);
        var d = 0;
        var g = d = "";
        c = {};
        for (index in RSGoPro_OFFERS[b].SORT_PROPS) if (g = RSGoPro_OFFERS[b].SORT_PROPS[index], c[g] = a.parents(".js-element").find(".prop_" + g).find(".div_option.selected").data("value"), g == f) {
            d = parseInt(index) + 1;
            d = RSGoPro_OFFERS[b].SORT_PROPS[d] ? RSGoPro_OFFERS[b].SORT_PROPS[d] : !1;
            break
        }
        if (d) {
            f = !0;
            var h = [];
            for (offerID in RSGoPro_OFFERS[b].OFFERS) {
                f = !0;
                for (pCode1 in c) c[pCode1] != RSGoPro_OFFERS[b].OFFERS[offerID].PROPERTIES[pCode1] &&
                (f = !1);
                f && h.push(RSGoPro_OFFERS[b].OFFERS[offerID].PROPERTIES[d])
            }
            a.parents(".js-element").find(".prop_" + d).find(".div_option").each(function (a) {
                a = $(this);
                var b = !0;
                for (inden in h) if (a.data("value") == h[inden]) {
                    b = !1;
                    break
                }
                a.addClass("disabled");
                b || a.removeClass("disabled")
            });
            a = a.parents(".js-element").find(".prop_" + d).find(".div_option.selected").hasClass("disabled") ? a.parents(".js-element").find(".prop_" + d).find(".div_option:not(.disabled):first") : a.parents(".js-element").find(".prop_" + d).find(".div_option.selected");
            RSGoPro_OffersExt_PropChanged(a)
        } else RSGoPro_OffersExt_ChangeHTML(a.parents(".js-element"))
    }
}

$(document).ready(function () {
    $(document).on("click", ".div_option", function (a) {
        a.stopPropagation();
        clearTimeout(RSGoPro_OffersExt_timeout_id);
        var b = $(this);
        if (!b.hasClass("disabled")) {
            var f = b.parents(".js-element").data("elementid");
            0 < f ? (b.parents(".offer_prop").data("code"), b.data("value"), RSGoPro_OFFERS[f] ? RSGoPro_OffersExt_PropChanged(b) : (RSGoPro_Area2Darken(b.parents(".js-element"), "animashka"), $.getJSON(SITE_DIR + "catalog/?AJAX_CALL=Y&action=get_element_json&element_id=" + f, {}, function (a) {
                RSGoPro_OFFERS[f] =
                    a;
                RSGoPro_OffersExt_PropChanged(b);
                RSGoPro_Area2Darken(b.parents(".js-element"))
            }).fail(function (a) {
                console.warn("Get element JSON -> fail request");
                RSGoPro_Area2Darken(b.parents(".js-element"))
            }))) : console.warn("Get element JSON -> element_id is empty")
        }
        return !1
    });
    $(document).on("click", ".div_selected", function (a) {
        $(".offer_prop.opened:not(.prop_" + $(this).parents(".offer_prop").data("code") + ")").removeClass("opened").addClass("closed");
        $(this).parents(".offer_prop").hasClass("closed") ? $(this).parents(".offer_prop").removeClass("closed").addClass("opened") :
            $(this).parents(".offer_prop").removeClass("opened").addClass("closed");
        return !1
    });
    $(document).on("click", function (a) {
        0 < $(a.target).parents(".offer_prop").length && !$(a.target).parents(".offer_prop").hasClass("color") || $(".offer_prop").removeClass("opened").addClass("closed")
    })
});