<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Условия оплаты");
$APPLICATION->SetPageProperty("description", "Условия оплаты. Мы предлагаем широкий ассортимент качественного инструмента по адекватным ценам.");
$APPLICATION->SetTitle("Условия оплаты");
?><h1>Как оплатить и заказать товар в компании "Белай Тойс"?</h1>
<p>
	 После регистрации на нашем сайте вам будут доступны розничные цены. Для получения оптовых цен свяжитесь с нашими менеджерами.
</p>
<h2>Как заказать</h2>
<p>
	 Для заказа товара необходимо зарегистрироваться на нашем сайте.Чтобы сделать заказ через каталог продукции, нажмите на кнопку «Заказать» рядом с необходимым вам наименованием. Товар будет добавлен в список покупок. Вы в любой момент можете просмотреть свой список на странице&nbsp;«Корзина». В таблице вы можете изменять количество заказанных товаров или удалять ненужные позиции. Чтобы перейти к процедуре оформления заказа нажмите на кнопку «Отправить».
</p>
<h2>Оплата</h2>
<p>
	 Оплатить выбранные товары можно следующими способами:
</p>
<ul>
	<li>Юридические лица и ИП<br>
	 Наличный и безналичный расчет.</li>
</ul>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>