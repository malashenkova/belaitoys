<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Получить прайс-лист");
?>
<script type="text/javascript">
	function request(){
		var w = $( window ).width();
			h = $( window ).height();			
			$("<div style='width:"+w+"px;height:"+h+"px;z-index:1000;background: rgba(255, 255, 255, 0.53);position: fixed;top: 0;left: 0;right: 0;bottom: 0;'></div>").insertBefore('.wrapper');
		$.ajax({
			url:'/local/php_interface/ajax/export_price_list.php',			
			type: "POST",
			dataType:'json',
			success:function(data){
				$('.price-link').find('a').attr('href', data.file);
				$('.price-link').find('a')[0].click();
				document.location.reload();
			}
		});	

		$(".info-request").html("Идет процес подготовки файла прайс листа. Пожалуйста подождите.");
	}
</script>
<p class="info-request"></p>
<div style="max-width: 200px" class="wides">
	<span class="button big_btn bold type_block" onclick="request()">Получить прайс</span>
</div>
<div class='price-link' style="display: none;"><a href="#" download>Скачать прайс-лист</a></div>
<??>
<?
/*echo $_SERVER["DOCUMENT_ROOT"]."<br>";
echo $_SERVER["HTTP_HOST"]."<br>";
echo $_SERVER["SERVER_NAME"]."<br>";
echo $_SERVER["SERVER_PROTOCOL"]."<br>";*/
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>