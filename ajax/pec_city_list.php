<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(!CModule::IncludeModule("iblock")){
	echo "failure";
	return;
}

$json_url = "http://www.pecom.ru/ru/calc/towns.php";
$json = file_get_contents($json_url);
$data = json_decode($json, TRUE);

$cityBase = Array();
foreach($data as $city) {
    $cityBase[] = array_shift($city);
}

foreach($cityBase as $city) {
    $bs = new CIBlockSection;
    $arFields = Array(
        "ACTIVE" => "Y",
        "IBLOCK_ID" => 34,
        "NAME" => $city,
    );
    $SECTION_ID = $bs->Add($arFields);

    $el = new CIBlockElement;

    $arLoadProductArray = Array(
    "MODIFIED_BY"    => $USER->GetID(),
    "IBLOCK_SECTION_ID" => $SECTION_ID,
    "IBLOCK_ID"      => 34,
    "NAME"           => "Прима Тойс",
    "ACTIVE"         => "Y"
    );

    $PRODUCT_ID = $el->Add($arLoadProductArray);
}