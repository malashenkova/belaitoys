<?
session_start();
header('Content-Type: application/json');

if(!empty($_POST['close'])) {
	$_SESSION['close'] = $_POST['close'];
	$closeBanner = false;
	if($_SESSION['close'] == 'yes') {
		$closeBanner = true;
	}
	echo json_encode($closeBanner);
}