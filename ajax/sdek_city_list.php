<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(!CModule::IncludeModule("iblock")){
	echo "failure";
	return;
}

$url = "http://integration.cdek.ru/pvzlist/v1/xml?allowedcod=1";
$xml = simplexml_load_file($url);

function xml2array ( $xmlObject, $out = array () )
{
    foreach ( (array) $xmlObject as $index => $node )
        $out[$index] = ( is_object ( $node ) ) ? xml2array ( $node ) : $node;
    return $out;
}
$base = xml2array($xml, $out);

echo "<pre>";

#print_r($cityBase);

foreach($base as $region) {
    foreach($region as $city) {
        $ID = 0;
        $cityData = xml2array($city);
       // echo "<pre>"; print_r($cityData); echo "</pre><hr>";
        
        $checkSectionRes = CIBlockSection::GetList(
            Array("SORT"=>"ASC"),
            Array("NAME" => $cityData["@attributes"]["RegionName"], "IBLOCK_ID" => 34),
            false,
            Array(),
            false
        );

        $checkSection = $checkSectionRes->GetNext();
        $ID = $checkSection;

        $bs = new CIBlockSection;
        $arFields = Array(
            "ACTIVE" => "Y",
            "IBLOCK_ID" => 34,
            "NAME" => $cityData["@attributes"]["RegionName"],
        );

        if($ID > 0)
        {
        $res = $bs->Update($ID, $arFields);
        }
        else
        {
        $ID = $bs->Add($arFields);
        $res = ($ID>0);
        }

        $SECTION_ID = $ID["ID"];
        #$SECTION_ID = $res["ID"];

        if($SECTION_ID > 1) {

            echo $cityData["@attributes"]["Name"];

            $cityElRes = CIBlockElement::GetList(
                Array("SORT"=>"ASC"),
                Array("IBLOCK_ID" => 34, "NAME" => "Прима Тойс ".$cityData["@attributes"]["Name"], "SECTION_ID" => $SECTION_ID),
                false,
                false,
                Array("NAME")
            );

            $cityEl = $cityElRes->GetNext();
            
            if($cityEl["NAME"]) {
                echo "City has added";
            } else {
                $el = new CIBlockElement;

                $prop = Array();
                $prop[757] = $cityData["@attributes"]["FullAddress"]; // Address
                $prop[758] = "8 (800) 201-36-56"; // Phone
                $prop[759] = "info@primatoys.ru"; // Email
                $prop[760] = "С 9:00 до 17:00 пн-пт, сб вск выходной"; // WorkTime
                $prop[761] = $cityData["@attributes"]["MetroStation"]; // Metro
                $prop[762] = $cityData["@attributes"]["coordY"].", ".$cityData["@attributes"]["coordX"]; // Cords

                $arLoadProductArray = Array(
                "MODIFIED_BY"    => $USER->GetID(),
                "IBLOCK_SECTION_ID" => $SECTION_ID,
                "IBLOCK_ID"      => 34,
                "NAME"           => "Прима Тойс ".$cityData["@attributes"]["Name"],
                "ACTIVE"         => "Y",
                "PROPERTY_VALUES"=> $prop
                );

                $PRODUCT_ID = $el->Add($arLoadProductArray);
            }
        }

    }
}

echo "<pre>";
