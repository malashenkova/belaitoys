<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(!CModule::IncludeModule("catalog") || !CModule::IncludeModule("iblock")){
	echo "failure";
	return;
}

if($_REQUEST["action"] == 'sameChange') {
	if($_REQUEST["place"] == 'section') {
		$res = CIBlockElement::GetByID($_REQUEST["ID"]);
		$arRes = $res->GetNext();
		if($arRes["DETAIL_PICTURE"]) $picture = CFile::ResizeImageGet($arRes["DETAIL_PICTURE"], Array("width" => 300, "height" => 300));
		$picture = $picture["src"];
		$arRes["IMAGE"] = $picture;
		
		$props = CIBlockElement::GetProperty($arRes["IBLOCK_ID"], $arRes["ID"], array("sort" => "asc"), Array("CODE"=>"CML2_ARTICLE"));
		$arProps = $props->Fetch();
		$articul = $arProps["VALUE"];
		$arRes["ARTICUL"] = $articul;
		
		$dbProductPrice = CPrice::GetListEx(
			array(),
			array("PRODUCT_ID" => $arRes["ID"], "CATALOG_GROUP_ID" => 30),
			false,
			false,
			array("ID", "CATALOG_GROUP_ID", "PRICE", "CURRENCY", "QUANTITY_FROM", "QUANTITY_TO")
		);
		$arPrice = $dbProductPrice->Fetch();
		$price = $arPrice["PRICE"];
		$price = CurrencyFormat($arPrice["PRICE"], $arPrice["CURRENCY"]);
		$arRes["PRICE"] = $price;
		
		echo json_encode($arRes);
	}
	if($_REQUEST["place"] == 'fastView') {
		$res = CIBlockElement::GetByID($_REQUEST["ID"]);
		$arRes = $res->GetNext();
		if($arRes["DETAIL_PICTURE"]) $picture = CFile::ResizeImageGet($arRes["DETAIL_PICTURE"], Array("width" => 300, "height" => 300));
		$picture = $picture["src"];
		$arRes["IMAGE"] = $picture;

		if($arRes["DETAIL_PICTURE"]) $bigPicture = CFile::ResizeImageGet($arRes["DETAIL_PICTURE"], Array("width" => 1800, "height" => 1800));
		$bigPicture = $bigPicture["src"];
		$arRes["BIG_IMAGE"] = $bigPicture;
		
		$props = CIBlockElement::GetProperty($arRes["IBLOCK_ID"], $arRes["ID"], array("sort" => "asc"), Array("CODE"=>"CML2_ARTICLE"));
		$arProps = $props->Fetch();
		$articul = $arProps["VALUE"];
		$arRes["ARTICUL"] = $articul;
		
		$dbProductPrice = CPrice::GetListEx(
			array(),
			array("PRODUCT_ID" => $arRes["ID"], "CATALOG_GROUP_ID" => 30),
			false,
			false,
			array("ID", "CATALOG_GROUP_ID", "PRICE", "CURRENCY", "QUANTITY_FROM", "QUANTITY_TO")
		);
		$arPrice = $dbProductPrice->Fetch();
		$price = $arPrice["PRICE"];
		$price = CurrencyFormat($arPrice["PRICE"], $arPrice["CURRENCY"]);
		$arRes["PRICE"] = $price;


		
		echo json_encode($arRes);
	}
	if($_REQUEST["place"] == 'item') {
		$res = CIBlockElement::GetByID($_REQUEST["ID"]);
		$arRes = $res->GetNext();
		if($arRes["DETAIL_PICTURE"]) $picture = CFile::ResizeImageGet($arRes["DETAIL_PICTURE"], Array("width" => 2000, "height" => 2000));
		$picture = $picture["src"];
		$arRes["IMAGE"] = $picture;
		
		$props = CIBlockElement::GetProperty($arRes["IBLOCK_ID"], $arRes["ID"], array("sort" => "asc"), Array("CODE"=>"CML2_ARTICLE"));
		$arProps = $props->Fetch();
		$articul = $arProps["VALUE"];
		$arRes["ARTICUL"] = $articul;
		
		$dbProductPrice = CPrice::GetListEx(
			array(),
			array("PRODUCT_ID" => $arRes["ID"], "CATALOG_GROUP_ID" => 30),
			false,
			false,
			array("ID", "CATALOG_GROUP_ID", "PRICE", "CURRENCY", "QUANTITY_FROM", "QUANTITY_TO")
		);
		$arPrice = $dbProductPrice->Fetch();
		$price = $arPrice["PRICE"];
		$price = CurrencyFormat($arPrice["PRICE"], $arPrice["CURRENCY"]);
		$arRes["PRICE"] = $price;
		
		echo json_encode($arRes);
	}
}

